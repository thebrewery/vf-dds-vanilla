import 'dotenv/config'
import { getParameters } from 'codesandbox/lib/api/define.js'
import fs from 'fs'
import axios from 'axios'
import FormData from 'form-data'

import moduleMap from '../src/module-map.js'

type FileType = {
  'index.html': {
    content: string
  }
}

type CssType = {
  [key: string]: {
    content: string
  }
}

type JsType = {
  [key: string]: {
    content: string
  }
}

type BinaryType = {
  [key: string]: {
    isBinary: boolean
    content: string
  }
}

let componentData: {
  componentName: string
  mdFile?: string
  sandbox_id?: string
  assets?: {
    path: string
    type: string
  }[]
  variants?: {
    componentName: string
    sandbox_id: string
    mdFile: string
  }[]
}[] = []

console.log('started export')

const imgMimeTypes = ['.svg', '.jpeg', '.jpg', '.png', '.mp4']
const unpkgUrl =
  process.env.IMG_LINK || 'https://unpkg.com/@vf-dds/vf-dds-vanilla'
const sandboxUrl = 'https://codesandbox.io/api/v1/sandboxes/define?json=1'
const examplePath = 'src/examples'
const tailwindHtmlPath = 'src/tailwind-components'
const tailwindCssPath = 'dist/tailwind.css'
const VARIANTS = 'variants'
const HTML = '.html'
const UTF = 'utf-8'
const tailwindCatalogues = fs
  .readdirSync(tailwindHtmlPath)
  .filter((catalogue) => catalogue !== '.DS_Store')
  .filter((catalogue) => catalogue !== 'README.md')
  .map((catalogue) => {
    return {
      path: catalogue,
      type: 'tailwind',
    }
  })

const allCatalogues = fs
  .readdirSync(examplePath)
  .filter((catalogue) => catalogue !== '.DS_Store')
  .filter((catalogue) => catalogue !== 'README.md')
  .map((catalogue) => {
    return {
      path: catalogue,
      type: 'normal',
    }
  })
  .concat(tailwindCatalogues)

const jsHandler = (jsSearchPath: string) => {
  const jsString = fs.readFileSync(jsSearchPath, UTF)

  const functionName = jsString.match(/export default function\s+(\w+)/)?.[1]

  if (!functionName) return

  for (const [initiator, vfJsComponent] of Object.entries(moduleMap.default as Record<string, { name: string }>)) {
    if (vfJsComponent.name === functionName) {
      const finalString = jsString
        .replace('export default', '')
        .concat(formatInitiatorString(initiator, functionName))

      return finalString
    }
  }
}

const formatInitiatorString = (initiator: string, functionName: string) => `
document.addEventListener('DOMContentLoaded', () => {
  const domElements = document.querySelectorAll('${initiator}')

  domElements.forEach((domElement) => {
    ${functionName}(domElement)
  })
})
`

const handleBinaryFiles = (
  binaryFiles: string[],
  catalogue: string,
  exampleFolder: string,
  basePath: string = examplePath
) => {
  const imgFiles = {} as BinaryType

  binaryFiles.forEach((binaryFile) => {
    binaryFiles = binaryFiles.filter((el) => el.includes('.'))

    imgMimeTypes.forEach((mimeType) => {
      if (binaryFile.includes(mimeType)) {
        if (mimeType === '.svg') return

        return (imgFiles[binaryFile] = {
          isBinary: true,
          content: `${unpkgUrl}/${basePath}/${catalogue}/${exampleFolder}/${binaryFile}`,
        })
      }
    })

    if (binaryFile.includes('.svg')) {
      return (imgFiles[binaryFile] = {
        isBinary: true,
        content: fs.readFileSync(
          `${basePath}/${catalogue}/${exampleFolder}/${binaryFile}`,
          UTF
        ),
      })
    }
  })

  return imgFiles
}

const handleVariants = async (
  binaryFiles: string[],
  componentName: string,
  category: string,
  exampleFiles: string[],
  exampleFolders: string[],
  jsSearchPaths: string[],
  cssFileContent: string,
  cssPath: string,
  isTailwind: boolean = false
) => {
  const files = {} as FileType

  const cssFiles = {} as CssType

  const jsFiles = {} as JsType

  cssFiles[isTailwind ? 'tailwind.css' : 'main.css'] = {
    content: cssFileContent,
  }

  const imgFiles = handleBinaryFiles(binaryFiles, category, componentName)

  jsSearchPaths.forEach((searchPath) => {
    if (fs.existsSync(searchPath)) {
      const finalString = jsHandler(searchPath)

      const jsName = searchPath.split(`${componentName}/`)[1]

      jsFiles[jsName ?? 'index.js'] = {
        content: finalString!,
      }
    }
  })

  const partialsArray = await Promise.all(
    exampleFolders.map((folder) => {
      const variantsFolderFiles = fs.readdirSync(
        `${examplePath}/${category}/${componentName}/${VARIANTS}/${folder}`
      )

      const layoutFileHtml = exampleFiles.filter((el) =>
        [HTML].every((type) => el.includes(type))
      )

      let layoutFileString = fs.readFileSync(
        `${examplePath}/${category}/${componentName}/${layoutFileHtml}`,
        UTF
      )

      const mdFile = variantsFolderFiles.filter((file) => !file.includes(HTML))

      const partialFile = variantsFolderFiles.filter((file) =>
        file.includes(HTML)
      )

      const partialFileContent = fs.readFileSync(
        `${examplePath}/${category}/${componentName}/${VARIANTS}/${folder}/${partialFile}`,
        UTF
      )

      const parsedMdFile = fs.readFileSync(
        `${examplePath}/${category}/${componentName}/${VARIANTS}/${folder}/${mdFile}`,
        UTF
      )

      const joinedFile = layoutFileString.replace(
        '{{content}}',
        partialFileContent
      )

      files['index.html'] = {
        content: joinedFile,
      }

      const parameters = getSBParameters(files, cssFiles, jsFiles, imgFiles)
      const nameOfFile = folder

      return postToSandbox(parameters, nameOfFile, parsedMdFile)
    })
  )

  const obj = {
    componentName: componentName,
    variants: partialsArray,
    assets: [
      { path: cssPath, type: 'css' },
      ...jsSearchPaths.map((searchPath) => {
        return { path: searchPath, type: 'js' }
      }),
    ].filter(Boolean) as { path: string; type: string }[],
  }

  componentData.push(obj)
}

const getSBParameters = (
  files: FileType,
  cssFiles: CssType,
  jsFiles: JsType,
  imgFiles: BinaryType
) => {
  const parameters = getParameters({
    files: {
      'package.json': {
        // @ts-ignore
        content: {
          type: 'module',
          name: 'sandbox',
          main: 'index.html',
          author: 'liroliro',
          license: 'ISC',
          description:
            'A visual representation of a component from the Vattenfall DDS.',
          dependencies: {
            codesandbox: '^2.2.3',
          },
        },
      },
      'sandbox.config.json': {
        // @ts-ignore
        content: { template: 'static' },
      },
      ...files,
      ...cssFiles,
      ...jsFiles,
      ...imgFiles,
    },
  })

  return parameters
}

const postToSandbox = async (
  parameters: string,
  componentName: string,
  mdFile?: string
) => {
  let reqBody = new FormData()
  reqBody.append('parameters', parameters)

  return await axios
    .post(sandboxUrl, reqBody, {
      headers: reqBody.getHeaders(),
    })
    .then(({ data }) => {
      const { sandbox_id } = data as { sandbox_id: string }

      const finalObject = {
        componentName,
        sandbox_id,
        mdFile: mdFile ?? '',
      }

      return finalObject
    })
    .catch((error) => {
      console.error('error', componentName, error)
      throw error
    })
}


const sandboxExports: Promise<any>[] = [];

for (let i = 0; i < allCatalogues.length; i++) {
  const category = allCatalogues[i].path
  const categoryType = allCatalogues[i].type as 'normal' | 'tailwind'

  let exampleFolders =
    categoryType === 'tailwind'
      ? fs.readdirSync(`${tailwindHtmlPath}/${category}`)
      : fs
          .readdirSync(`${examplePath}/${category}`)
          .filter((innerCatalogue) => innerCatalogue !== '.DS_Store')

  for (let y = 0; y < exampleFolders.length; y++) {
    const componentName = exampleFolders[y]
    const cssPath =
      categoryType === 'normal'
        ? `src/style/component-css/${componentName}.css`
        : tailwindCssPath
    const cssFileContent = fs.readFileSync(cssPath, UTF)

    const exampleFiles =
      categoryType === 'tailwind'
        ? fs.readdirSync(`${tailwindHtmlPath}/${category}/${componentName}`)
        : fs.readdirSync(`${examplePath}/${category}/${componentName}`)

    let binaryFiles = exampleFiles.filter((el) =>
      imgMimeTypes.some((type) => el.includes(type))
    )

    let textFiles = exampleFiles.filter(
      (el) => !imgMimeTypes.some((type) => el.includes(type))
    )

    const jsSearchPaths: string[] = []
    const jsBasePath = `src/components/${category}/${componentName}`

    if (fs.existsSync(jsBasePath)) {
      const allFiles = fs.readdirSync(jsBasePath)
      allFiles.map((jsPath) => jsSearchPaths.push(`${jsBasePath}/${jsPath}`))
    } else {
      if (fs.existsSync(`${jsBasePath}.js`)) {
        jsSearchPaths.push(`${jsBasePath}.js`)
      }
    }

    if (exampleFiles.some((file) => file === VARIANTS)) {
      const folderName = textFiles.filter((file) => !file.includes('.'))

      const exampleFolders = fs.readdirSync(
        `${examplePath}/${category}/${componentName}/${folderName}`
      )

      handleVariants(
        binaryFiles,
        componentName,
        category,
        exampleFiles,
        exampleFolders,
        jsSearchPaths,
        cssFileContent,
        cssPath,
        categoryType === 'tailwind'
      )
    } else {
      // hacky way for now to workaround folders inside the main folders
      textFiles = textFiles.filter((el) => el.includes('.'))

      const files = {} as FileType

      const cssFiles = {} as CssType

      const jsFiles = {} as JsType

      textFiles.forEach((textFile) => {
        files['index.html'] = {
          content:
            categoryType === 'tailwind'
              ? fs.readFileSync(
                  `${tailwindHtmlPath}/${category}/${componentName}/${textFile}`,
                  UTF
                )
              : fs.readFileSync(
                  `${examplePath}/${category}/${componentName}/${textFile}`,
                  UTF
                ),
        }
      })

      if (categoryType === 'tailwind') {
        textFiles.forEach(() => {
          cssFiles['tailwind.css'] = {
            content: cssFileContent,
          }
        })
      } else {
        textFiles.forEach(() => {
          cssFiles['main.css'] = {
            content: cssFileContent,
          }
        })
      }

      jsSearchPaths.forEach((searchPath) => {
        if (fs.existsSync(searchPath)) {
          const finalString = jsHandler(searchPath)

          const jsName = searchPath.split(`${componentName}/`)[1]

          jsFiles[jsName ?? 'index.js'] = {
            content: finalString!,
          }
        }
      })

      const imgFiles = handleBinaryFiles(
        binaryFiles,
        category,
        componentName,
        allCatalogues[i].type === 'tailwind' ? tailwindHtmlPath : examplePath
      )
      const parameters = getSBParameters(files, cssFiles, jsFiles, imgFiles)

      const task = postToSandbox(parameters, componentName).then((sandboxData) => {
        const modifiedData = {
          ...sandboxData,
          assets: [
            { path: cssPath, type: 'css' },
            ...jsSearchPaths.map((searchPath) => {
              return { path: searchPath, type: 'js' }
            }),
          ].filter(Boolean) as { path: string; type: string }[],
        }

        componentData.push(modifiedData)

        fs.writeFileSync(
          'sandbox/component-data.json',
          JSON.stringify(componentData)
        )
      })
      sandboxExports.push(task);
    }
  }
}

Promise.all(sandboxExports).then(() => console.log(`Finished exporting ${sandboxExports.length} sandboxes to CodeSandbox. sandbox/component-data.json replaced with new urls`))
