import { resolve } from 'path'
import { defineConfig } from 'vite'
import mix from 'vite-plugin-mix'
// import * as url from 'url'
// const dirname = url.fileURLToPath(new URL('.', import.meta.url))

/** @type {import("vite").defineConfig } */
export default defineConfig(({ command }) => {
  return {
    plugins: [
      command === 'serve' &&
        mix({
          handler: './server.ts',
        }),
    ].filter(Boolean),

    build: {
      outDir: './dist',
      lib: {
        entry: resolve(__dirname, 'src/index.mjs'),
        name: 'vfDdsJs',
        fileName: () => 'modules.js',
        formats: ['iife'],
      },
    },
  }
})
