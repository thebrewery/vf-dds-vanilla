> :warning: 0.14.1 will **require** updates for JavaScript implementation, read more at the [DDS](https://digitaldesign.vattenfall.com/articles/updating-to-version-0-14).

# Vanilla part of Vattenfall DDS

This is the Vanilla JavaScript part of Vattenfall Digital Design System (DDS). Here you can look through logic, styling and other things layout-wise.

## Contact and issue reporting

Contact us with general or specific questions at [dds@se.nordddb.com](mailto:dds@se.nordddb.com).
To handle our issue reporting, we are using an external tool. To report an issue, please use [this form](https://digitaldesign.vattenfall.com/contact).

## Installation

- npm install

To build css/Tailwind for examples

- npm run build-sass

To compile sandbox ids

- npm run compile-sandbox

To build production for deployment

- npm run compile-all

If you want an overview of your components, use `npm run dev`. This is built using vite, and code is stored in `server.ts`.

### Standards

This project uses certain techniques, such as

- BEM - https://en.bem.info/methodology/quick-start/
- semver - https://semver.org/

## Structure

The project is built with the following thoughts:

- Components should always have their own css file, named after the component. E.g. css related to `buttons`, should be called `buttons.css`.
- Components should always have their css stored in a scss-file named after it's parent. Example - css related to `buttons` should be in `indicators.scss`.
- Components who need JavaScript, should have their own JavaScript file named after component. E.g. JavaScript related to `people` should be contained in `people.js`. If no JavaScript is supplied, none is needed.
- Fonts are stored at `src/fonts`.
- Global variables can be found in `src/style/_variables.scss`
- All styling is finally summarized in `src/style.scss`, which exports to `src/style.css`.
- In the examples folder (`src/examples`), you can find example of how the components are used, in code.
- If you want to view an example of a component working, you can start our local development environment with `npm run dev`, and open the corresponding component in the UI.

## Adding new components

In case you want to add new component to the library, follow the start underneath.

### Javascript incorporation

If you want to add a new Javascript component, there are some things needed to get things working.

### Adding and compiling CSS

Components in this library contains mostly css and some JavaScript. In case a new component is to be created, you should:

- Add a scss-file in to the correct map, i.e. `src/components/style/modules/{component_name}.scss`, which contains the styling
- Add in to a category-named file as a @forward, i.e `src/components/style/modules/modules.scss`.
- Add it to the scss-script (/scripts/compile-all-scss.sh), to be able to compile new css-files.
- Write example in `src/examples`, and check if it compiles with a new CodeSandbox id (using `npm run compile-sandbox`).

We write the styling for the site using SCSS but we compile it using `sass`. We have a script contained in `/scripts` named `compile-all-scss.sh`, which will create the category level, component level and global css-files needed. Run this from the root with `./scripts/compile-all-scss.sh`.

### Adding Tailwind components

Instead of adding your example to the examples folder, you should add it into `src/tailwind-components`. You should follow the same structure of category/component name. JavaScript are added the same as vanilla components. To build the related CSS, use `npm run build-tailwind`.

### Initializing JavaScript

There are two main ways to initialize JavaScript, both are discussed [here](https://digitaldesign.vattenfall.com/articles/initializing-javascript-with-dds-version-14).

### CodeSandbox

We use CodeSandbox to visualize our examples, and all these are generated programmatically. The code used for this is located in `sandbox/sandbox.ts`, and ids are later exposed to our end users through the file `component-data.json`. Other then generating the ids/sandboxes we do some special handling of JS and CSS, to cater for CodeSandbox.
