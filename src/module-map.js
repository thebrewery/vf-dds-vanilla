import preloader from './components/indicators/preloader.js'
import tooltip from './components/dialogues/tooltip.js'
import theEdit from './components/campaign-assets/the-edit.js'
import progressIndicator from './components/indicators/progress-indicator.js'
import number from './components/input/number.js'
import expoCarousel from './components/modules/expo-carousel.js'
import people from './components/modules/people.js'
import singleSelect from './components/selection/option/singleSelect.js'
import multiSelect from './components/selection/option/multiSelect.js'
import slider from './components/selection/slider.js'
import underlineDropdown from './components/selection/dropdown/underline-dropdown.js'
import standardDropdown from './components/selection/dropdown/standard-dropdown.js'
import header from './components/navigation/header.js'
import checkboxDropdown from './components/selection/dropdown/checkbox-dropdown.js'
import value from './components/selection/value.js'
import tabs from './components/navigation/tabs.js'
import gdprConsent from './components/dialogues/gdpr-consent.js'
import notification from './components/dialogues/notification.js'
import carousel from './components/modules/carousel.js'
import upload from './components/modules/upload.js'
import calendar from './components/selection/calendar.js'

/**
  @type {
    Record<string, VfJsComponent>
  }
 */
const moduleMap = {
  '.vf-js-gdpr-consent': gdprConsent,
  '.vf-js-notification': notification,
  '.vf-js-preloader': preloader,
  '.vf-js-tooltip': tooltip,
  '.vf-js-the-edit': theEdit,
  '.vf-js-horizontal-steppers': progressIndicator,
  '.vf-js-number': number,
  '.vf-js-expo-carousel': expoCarousel,
  '.vf-js-people': people,
  '.vf-js-header': header,
  '.vf-js-checkbox-dropdown': checkboxDropdown,
  '.vf-js-standard-dropdown': standardDropdown,
  '.vf-js-underline-dropdown': underlineDropdown,
  '.vf-js-single-select': singleSelect,
  '.vf-js-multi-select': multiSelect,
  '.vf-js-slider': slider,
  '.vf-js-value': value,
  '.vf-js-tab-bar': tabs,
  '.vf-js-carousel': carousel,
  '.vf-js-upload': upload,
  '.vf-input': calendar,
}
export default moduleMap
