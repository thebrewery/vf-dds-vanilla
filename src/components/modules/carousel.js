export default function carousel(domElement) {
  let windowWidth = window.innerWidth
  const cardContainer = domElement.querySelector('.vf-carousel-card-container')
  const indicatorContainer = domElement.querySelector(
    '.vf-carousel-indicator-container'
  )
  const carouselContainer = domElement.querySelector('#vf-carousel-container')
  const isMobile = windowWidth < 576
  const isDesktop = windowWidth > 1400
  const centerOfWindow = windowWidth / 2
  const breakPoint = !isDesktop ? (isMobile ? 0.25 : 0.33) : 1

  const leftArrow = domElement.querySelector('#left-arrow')
  const rightArrow = domElement.querySelector('#right-arrow')
  leftArrow.addEventListener('click', previousSlide)
  rightArrow.addEventListener('click', nextSlide)

  function previousSlide() {
    const currentSlide = carouselContainer.querySelector('div:first-child')
    const cardWidth = currentSlide.offsetWidth
    const margin = window
      .getComputedStyle(currentSlide)
      .getPropertyValue('margin-right')
    const totalWidth = cardWidth + parseInt(margin)
    carouselContainer.scrollTo({
      left: carouselContainer.scrollLeft - totalWidth,
      behavior: 'smooth',
    })
  }

  function nextSlide() {
    const currentSlide = carouselContainer.querySelector('div:first-child')
    const cardWidth = currentSlide.offsetWidth
    const margin = window
      .getComputedStyle(currentSlide)
      .getPropertyValue('margin-right')
    const totalWidth = cardWidth + parseInt(margin)
    carouselContainer.scrollTo({
      left: carouselContainer.scrollLeft + totalWidth,
      behavior: 'smooth',
    })
  }

  // find index of the card that is visible to 100%
  // and use that to set the indicator to that card.
  const findActiveCard = (entries) => {
    return new Promise((resolve) => {
      const observer = new IntersectionObserver((entries) => {
        entries.forEach((entry) => {
          if (entry.isIntersecting) {
            if (entry.intersectionRatio === 1) {
              const visibleIndex = entry.target.getAttribute('index')
              observer.unobserve(entry.target)
              resolve(visibleIndex)
            }
            observer.unobserve(entry.target)
          }
        })
      })
      observer.observe(entries)
    })
  }

  const addOverlay = (entries, observer) => {
    entries.forEach((entry) => {
      // dont add overlay to cards on the left hand side of the screen.
      if (centerOfWindow < entry.intersectionRect.right) {
        if (entry.isIntersecting) {
          if (entry.intersectionRatio < breakPoint) {
            entry.target.classList.add('vf-carousel-next-slide-overlay')
          } else {
            entry.target.classList.remove('vf-carousel-next-slide-overlay')
          }
        }
        observer.unobserve(entry.target)
      }
    })
  }
  let options = {
    root: carouselContainer,
    rootMargin: '0px',
  }

  const observer = new IntersectionObserver(addOverlay, options)

  // create indicator dots
  for (let i = 0; i < cardContainer.length; i++) {
    if (i === 0) {
      // add active class to first indicator
      indicatorContainer.append(
        '<div class="vf-carousel-indicator vf-carousel-indicator-active"></div>'
      )
    } else {
      indicatorContainer.append('<div class="vf-carousel-indicator"></div>')
    }
  }

  // on load - first run
  // creat attribute index to every card and add intial overlay to card not 100% visible
  const cardContainers = carouselContainer.querySelectorAll('.cardContainer')
  for (let i = 0; i < cardContainers.length; i++) {
    const currentElement = cardContainers[i]
    let text = currentElement.querySelector('.vf-carousel-subtitle').textContent
    if (windowWidth < 1200 && text.length > 75) {
      currentElement.querySelector('.vf-carousel-subtitle').innerHTML =
        text.substring(0, 75) + '...'
    } else {
      currentElement.querySelector('.vf-carousel-subtitle').innerHTML =
        text.substring(0, 100) + '...'
    }
    currentElement.setAttribute('index', i)
    observer.observe(currentElement)
  }

  // runs when user scrolls in the carousel
  carouselContainer.addEventListener('scroll', async () => {
    const cardContainers = carouselContainer.querySelectorAll(
      '.vf-carousel-card-container'
    )
    for (let i = 0; i < cardContainers.length; i++) {
      const currentElement = cardContainers[i]
      observer.observe(currentElement)
      if (isMobile) {
        const index = await findActiveCard(currentElement)
        handleActiveIndicator(index)
      }
    }
  })

  // adds or remove class to indicator if the carousel card is in viewport too 100%
  function handleActiveIndicator(index) {
    domElement.querySelectorAll('.vf-carousel-indicator').forEach((el, i) => {
      let activeIndicator = el
      if (parseInt(index) === i) {
        activeIndicator.addClass('vf-carousel-indicator-active')
      } else {
        activeIndicator.removeClass('vf-carousel-indicator-active')
      }
    })
  }

  return () => {
    leftArrow.removeEventListener('click', previousSlide)
    rightArrow.removeEventListener('click', nextSlide)

    carouselContainer.removeEventListener('scroll', async () => {
      const cardContainers = carouselContainer.querySelectorAll(
        '.vf-carousel-card-container'
      )
      for (let i = 0; i < cardContainers.length; i++) {
        const currentElement = cardContainers[i]
        observer.observe(currentElement)
        if (isMobile) {
          const index = await findActiveCard(currentElement)
          handleActiveIndicator(index)
        }
      }
    })
  }
}
