export default function people(domElement) {
  const innerChildren = domElement.querySelectorAll('.vf-people-inner')
  const indicatorContainer = domElement.querySelector('.vf-people__indicators')
  const desktopArrows = domElement.querySelectorAll('.vf-people__arrow')
  const indicatorStyle =
    'h-[8px] w-[8px] rounded-[100%] bg-medium-grey inline-block transition-colors'

  const observer = new IntersectionObserver(
    (entries) => {
      entries.forEach((entry) => {
        if (entry.isIntersecting) {
          const activeElements = domElement.querySelectorAll('.bg-active')
          activeElements.forEach((element) => {
            element.classList.remove('bg-active')
          })
          const index = entry.target.dataset.index
          const indicator = indicatorContainer.querySelector(
            `[data-index="${index}"]`
          )
          indicator.className += ' bg-active'
        }
      })
    },
    {
      root: domElement,
      rootMargin: '0px',
      threshold: 0.5,
    }
  )

  innerChildren.forEach((child, index) => {
    child.dataset.index = index
    observer.observe(child)
  })

  const arrowClickFunction = (arrow) => {
    const direction = arrow.dataset.direction

    if (direction === 'backward') {
      const activeElement = domElement.querySelector('.bg-active')
      const index = parseInt(activeElement.dataset.index)
      const elementToScrollTo = innerChildren[index - 1]

      if (!elementToScrollTo) {
        const lastElement = innerChildren[innerChildren.length - 1]

        lastElement.scrollIntoView({
          behavior: 'smooth',
          block: 'start',
          inline: 'nearest',
        })
      }

      elementToScrollTo.scrollIntoView({
        behavior: 'smooth',
        block: 'start',
        inline: 'nearest',
      })
    }

    if (direction === 'forward') {
      const activeElement = domElement.querySelector('.bg-active')
      const index = parseInt(activeElement.dataset.index)
      const elementToScrollTo = innerChildren[index + 1]

      if (!elementToScrollTo) {
        const firstElement = innerChildren[0]

        firstElement.scrollIntoView({
          behavior: 'smooth',
          block: 'start',
          inline: 'nearest',
        })
      }

      elementToScrollTo.scrollIntoView({
        behavior: 'smooth',
        block: 'start',
        inline: 'nearest',
      })
    }
  }

  desktopArrows.forEach((arrow) => {
    arrow.addEventListener('click', () => {
      arrowClickFunction(arrow)
    })
  })

  const handleClick = (clickedSpan) => {
    const index = clickedSpan.dataset.index
    const elementToScrollTo = innerChildren[index]

    elementToScrollTo.scrollIntoView({
      behavior: 'smooth',
      block: 'start',
      inline: 'nearest',
    })
  }

  const renderIndicators = () => {
    innerChildren.forEach((_, index) => {
      const span = document.createElement('span')
      span.className = indicatorStyle
      span.dataset.index = index
      indicatorContainer.appendChild(span)
      span.addEventListener('click', () => handleClick(span))
    })
  }

  renderIndicators()

  return () => {
    innerChildren.forEach((child) => {
      observer.unobserve(child)
    })

    desktopArrows.forEach((arrow) => {
      arrow.removeEventListener('click', arrowClickFunction(arrow))
    })

    innerChildren.forEach((_, index) => {
      const span = document.querySelector(`span[index="${index}"]`)
      span.removeEventListener('click', handleClick(span))
    })
  }
}
