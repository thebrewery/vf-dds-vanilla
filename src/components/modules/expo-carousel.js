export default function expoCarousel(domElement) {
  const media = domElement.querySelectorAll('.vf-expo-slide__media')
  const allCarouselItems = domElement.querySelectorAll(
    '.vf-expo-carousel__observer'
  )
  const carouselWrapper = domElement.querySelector('.vf-expo-carousel__wrapper')
  let allButtons
  let activeImage
  const prevArrow = document.createElement('span')
  prevArrow.classList.add('vf-expo-carousel__arrow--prev')
  const nextArrow = document.createElement('span')
  nextArrow.classList.add('vf-expo-carousel__arrow--next')

  media.forEach((_, index) => {
    const span = document.createElement('span')

    span.classList.add('vf-expo-carousel__button')
    span.textContent = index + 1

    const buttonContainer = domElement.querySelector(
      '.vf-expo-carousel__buttons'
    )
    buttonContainer.appendChild(span)
    allButtons = domElement.querySelectorAll('.vf-expo-carousel__button')
  })

  if (window.ScrollTimeline) {
    allCarouselItems.forEach((item) => {
      item.querySelector('.vf-expo-slide__media').animate(
        {
          transform: ['translateX(-50%)', 'translateX(0)', 'translateX(50%)'],
        },
        {
          timeline: new ScrollTimeline({
            fill: 'both',
            timeRange: 1,
            orientation: 'horizontal',
            source: carouselWrapper,
            scrollSource: carouselWrapper,
            scrollOffsets: [
              {
                target: item,
                edge: 'end',
                threshold: 0,
              },
              {
                target: item,
                edge: 'start',
                threshold: 0,
              },
            ],
          }),
        }
      )
    })
  }

  const createObservers = () => {
    const observer = new IntersectionObserver(
      (entries) => {
        const interSectingElement = entries.find(
          (entry) => entry.isIntersecting
        )
        const activeEntry = interSectingElement?.target.dataset.index

        entries.forEach((el) => {
          if (!el.isIntersecting) {
            el.target
              .querySelector('.vf-expo-slide__content-container')
              .classList?.remove('fade-in')
          } else {
            interSectingElement.target
              .querySelector('.vf-expo-slide__content-container')
              ?.classList.add('fade-in')
          }
        })

        if (activeEntry) {
          activeImage = activeEntry
          allButtons.forEach((button, index) => {
            if (parseInt(index) === parseInt(activeEntry)) {
              button.classList.add('active')
            } else {
              button.classList.remove('active')
            }
          })

          if (interSectingElement.target.querySelector('video')) {
            const video = interSectingElement.target.querySelector('video')
            video.play()
          } else if (carouselWrapper.querySelectorAll('video')) {
            const videoList = carouselWrapper.querySelectorAll('video')

            videoList.forEach((video) => video.pause())
          }
        }
      },
      {
        root: carouselWrapper,
        threshold: 0.5,
      }
    )

    const elements = carouselWrapper.querySelectorAll(
      '.vf-expo-carousel__observer'
    )

    elements.forEach((el, index) => {
      el.dataset.index = index
      observer.observe(el)
    })

    const buttonClick = (index) => {
      const relevantImg = domElement.querySelector(
        `.vf-expo-carousel__observer[data-index="${index}"]`
      )

      relevantImg.scrollIntoView({
        behavior: 'smooth',
        block: 'center',
        inline: 'start',
      })
    }

    allButtons.forEach((button, index) => {
      button.addEventListener('click', () => {
        buttonClick(index)
      })
    })

    if (domElement.dataset.autoscroll) {
      createAutoScroll()
    }

    if (domElement.dataset.showArrows) {
      createArrows()
    }
    return () => observer.disconnect()
  }

  const createAutoScroll = () => {
    const scrollTimer = domElement.dataset.autoScrollTimer || '3000'

    setInterval(() => {
      let nextImage
      if (parseInt(activeImage) === allCarouselItems.length - 1) {
        nextImage = carouselWrapper.querySelector(
          `.vf-expo-carousel__observer[data-index="0"]`
        )
      } else {
        nextImage = carouselWrapper.querySelector(
          `.vf-expo-carousel__observer[data-index="${
            parseInt(activeImage) + 1
          }"]`
        )
      }

      nextImage.scrollIntoView({
        behavior: 'smooth',
        block: 'center',
        inline: 'center',
      })
    }, scrollTimer)
  }

  const nextArrowClick = () => {
    const prevImage = carouselWrapper.querySelector(
      `.vf-expo-carousel__observer[data-index="${
        parseInt(activeImage) === 0
          ? parseInt(allCarouselItems.length) - 1
          : parseInt(activeImage) - 1
      }"]`
    )

    prevImage.scrollIntoView({
      behavior: 'smooth',
      block: 'center',
      inline: 'center',
    })
  }

  const prevArrowClick = () => {
    const prevImage = carouselWrapper.querySelector(
      `.vf-expo-carousel__observer[data-index="${
        parseInt(activeImage) === parseInt(allCarouselItems.length) - 1
          ? 0
          : parseInt(activeImage) + 1
      }"]`
    )

    prevImage.scrollIntoView({
      behavior: 'smooth',
      block: 'center',
      inline: 'center',
    })
  }

  const createArrows = () => {
    const leftArrowIcon = document.createElement('span')
    leftArrowIcon.classList.add('vf-icon-left')
    const rightArrowIcon = document.createElement('span')
    rightArrowIcon.classList.add('vf-icon-right')
    prevArrow.appendChild(leftArrowIcon)
    nextArrow.appendChild(rightArrowIcon)

    prevArrow.addEventListener('click', () => {
      nextArrowClick()
    })

    nextArrow.addEventListener('click', () => {
      prevArrowClick()
    })

    domElement.append(prevArrow, nextArrow)
  }

  createObservers()

  return () => {
    allButtons.forEach((button, index) => {
      button.removeEventListener('click', () => {
        buttonClick(index)
      })
    })
    prevArrow.removeEventListener('click', prevArrowClick)
    nextArrow.removeEventListener('click', nextArrowClick)
  }
}
