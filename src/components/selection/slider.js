export default function slider(domElement) {
  const fromValueDisplay = domElement.querySelector('#vf-value-from')
  const fromValueSlider = domElement.querySelector('#vf-value-slider-from')
  const defaultValues = domElement.querySelector('#vf-value-definition')

  let minValue
  let maxValue

  const setDefaultValues = () => {
    const {
      dataset: {
        minValue: minValueFromHTML = 0,
        maxValue: maxValueFromHTML = 1000,
        defaultValue = 0,
      },
    } = defaultValues

    fromValueSlider.min = minValueFromHTML
    fromValueSlider.max = maxValueFromHTML
    fromValueSlider.value = defaultValue ?? minValueFromHTML

    fromValueDisplay.value = defaultValue ?? minValueFromHTML

    minValue = minValueFromHTML
    maxValue = maxValueFromHTML
  }

  const getParsedValues = () => {
    const from = parseInt(fromValueSlider.value)

    return [from]
  }

  const fillSlider = () => {
    const rangeDistance = fromValueSlider.max - fromValueSlider.min
    const fromPosition = fromValueSlider.value - fromValueSlider.min
    const toPosition = fromValueSlider.value - fromValueSlider.min

    fromValueSlider.style.background = `linear-gradient(
      to right,
      #1964A3 0%,
      #1964A3 ${(fromPosition / rangeDistance) * 100}%,
      #1964A3 ${(toPosition / rangeDistance) * 100}%,
      transparent ${(toPosition / rangeDistance) * 100}%,
      transparent 100%)`
  }

  const handleFromSliderInput = () => {
    const [from] = getParsedValues()
    fromValueDisplay.value = from

    fillSlider()
  }

  const handleValueFromInput = (receivedValue) => {
    if (parseInt(receivedValue) <= parseInt(minValue)) {
      fromValueSlider.value = minValue
      fromValueDisplay.value = minValue
    }

    if (parseInt(receivedValue) >= parseInt(maxValue)) {
      fromValueSlider.value = maxValue
      fromValueDisplay.value = maxValue
    }

    fromValueSlider.value = receivedValue

    fillSlider()
  }

  setDefaultValues()
  fromValueSlider.oninput = () => handleFromSliderInput()
  fromValueDisplay.addEventListener('change', (e) => {
    handleValueFromInput(e.target.value)
  })
  fillSlider()

  return () => {
    fromValueDisplay.remove('change', (e) => {
      handleValueFromInput(e.target.value)
    })
  }
}
