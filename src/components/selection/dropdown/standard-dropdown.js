function searchParentForId(el, id) {
  while (el.parentNode) {
    el = el.parentNode

    if (el.id === id) return el
  }
  return null
}

export default function standardDropdown(wrapper) {
  wrapper.querySelectorAll('#vf_dropdown_standard').forEach((element) => {
    element.addEventListener('click', () => {
      element.classList.toggle('open')

      const list = element.querySelector('.vf-dropdown-list')
      list.classList.toggle('active')
    })
  })

  const listItemClick = (listItem) => {
    wrapper.querySelectorAll('span.vf-icon-check').forEach((el) => el.remove())

    let span = document.createElement('span')
    span.className = 'vf-icon-check'
    listItem.appendChild(span)

    const parent = searchParentForId(listItem, 'vf_dropdown_standard')

    const label = parent.querySelector('label#vf_label_standard')

    if (label) {
      label.innerHTML = listItem.textContent
    }
  }

  wrapper.querySelectorAll('.vf-dropdown-list-item').forEach((listItem) => {
    listItem.addEventListener('click', () => {
      listItemClick(listItem)
    })
  })

  return () => {
    wrapper.querySelectorAll('#vf_dropdown_standard').forEach((element) => {
      element.removeEventListener('click', () => {
        element.classList.toggle('open')

        const list = element.querySelector('.vf-dropdown-list')
        list.classList.toggle('active')
      })
    })

    wrapper.querySelectorAll('.vf-dropdown-list-item').forEach((listItem) => {
      listItem.removeEventListener('click', () => {
        listItemClick(listItem)
      })
    })
  }
}
