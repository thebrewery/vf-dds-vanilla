export default function checkboxDropdown(element) {
  let containerText = 0

  function searchParentForId(el, id) {
    while (el.parentNode) {
      el = el.parentNode

      if (el.id === id) return el
    }
    return null
  }

  element.addEventListener('click', () => {
    element.classList.toggle('open')

    const list = element.querySelector('.vf-dropdown-list__checkbox')
    list.classList.toggle('active')
  })

  const inputContainerClick = (e, inputContainer) => {
    const relatedInput = inputContainer.querySelector('input')
    const clickedTarget = e.target

    if (relatedInput.checked && clickedTarget.tagName !== 'INPUT') {
      relatedInput.checked = false
    } else if (clickedTarget.tagName !== 'INPUT') {
      relatedInput.checked = true
    }

    const inputs = element.querySelectorAll('#vf-dropdown__checkbox')

    const parent = searchParentForId(inputContainer, 'vf_dropdown_checkbox')

    const label = parent.querySelector('label#vf_label_checkbox')

    if (relatedInput.checked) {
      containerText++
    } else {
      containerText--
    }

    if (containerText === 0) {
      label.textContent = 'Select option'
    }

    if (containerText === 1) {
      label.textContent = relatedInput.value
    }

    if (containerText > 1 && containerText !== inputs.length) {
      label.textContent = `${containerText} Selected`
    }

    if (containerText === inputs.length) {
      label.textContent = 'All Selected'
    }
  }

  element
    .querySelectorAll('.vf-dropdown-list-item--with-checkbox')
    .forEach((inputContainer) => {
      inputContainer.addEventListener('click', (e) => {
        inputContainerClick(e, inputContainer)
      })
    })

  return () => {
    element.removeEventListener('click', () => {
      element.classList.toggle('open')

      const list = element.querySelector('.vf-dropdown-list__checkbox')
      list.classList.toggle('active')
    })
    element
      .querySelectorAll('.vf-dropdown-list-item--with-checkbox')
      .forEach((inputContainer) => {
        inputContainer.removeEventListener('click', inputContainerClick)
      })
  }
}
