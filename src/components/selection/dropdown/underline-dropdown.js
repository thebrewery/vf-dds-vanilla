function searchParentForId(el, id) {
  while (el.parentNode) {
    el = el.parentNode

    if (el.id === id) return el
  }
  return null
}

export default function underlineDropdown(domElement) {
  domElement.querySelectorAll('#vf_dropdown_underline').forEach((dropdown) => {
    dropdown.addEventListener('click', () => {
      dropdown.classList.toggle('open')
      const list = dropdown.querySelector('.vf-dropdown-list__underline')
      list.classList.toggle('active')
    })

    dropdown
      .querySelectorAll('.vf-dropdown-list-item__underline')
      ?.forEach((listItem) => {
        listItem.addEventListener('click', () => {
          const parent = searchParentForId(listItem, 'vf_dropdown_underline')
          const label = parent.querySelector('#vf_label_underline')

          domElement
            .querySelectorAll('span.vf-icon-check')
            .forEach((el) => el.remove())

          let span = document.createElement('span')
          span.className = 'vf-icon-check'
          listItem.appendChild(span)

          if (label) {
            label.innerHTML = listItem.textContent
          }
        })
      })
  })

  return () => {
    domElement
      .querySelectorAll('#vf_dropdown_underline')
      .forEach((dropdown) => {
        dropdown.remove('click', () => {
          dropdown.classList.toggle('open')
          const list = dropdown.querySelector('.vf-dropdown-list__underline')
          list.classList.toggle('active')
        })
      })
  }
}
