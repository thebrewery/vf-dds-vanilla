export default function value(domElement) {
  const fromValueDisplay = domElement.querySelector('#vf-value-from')
  const toValueDisplay = domElement.querySelector('#vf-value-to')
  const fromValueSlider = domElement.querySelector('#vf-value-slider-from')
  const toValueSlider = domElement.querySelector('#vf-value-slider-to')
  const defaultValues = domElement.querySelector('#vf-value-definition')

  let minValue
  let maxValue

  const setDefaultValues = () => {
    const {
      dataset: {
        minValue: minValueFromHTML = 0,
        maxValue: maxValueFromHTML = 1000,
        defaultValue = 0,
      },
    } = defaultValues

    fromValueSlider.min = minValueFromHTML
    fromValueSlider.max = maxValueFromHTML
    fromValueSlider.value = defaultValue ?? minValueFromHTML

    toValueSlider.min = minValueFromHTML
    toValueSlider.max = maxValueFromHTML
    toValueSlider.value = maxValueFromHTML

    fromValueDisplay.value = defaultValue ?? minValueFromHTML
    toValueDisplay.value = maxValueFromHTML

    minValue = minValueFromHTML
    maxValue = maxValueFromHTML
  }

  const getParsedValues = () => {
    const from = parseInt(fromValueSlider.value)
    const to = parseInt(toValueSlider.value)

    return [from, to]
  }

  const fillSlider = () => {
    const rangeDistance = toValueSlider.max - toValueSlider.min
    const fromPosition = fromValueSlider.value - toValueSlider.min
    const toPosition = toValueSlider.value - toValueSlider.min

    toValueSlider.style.background = `linear-gradient(
      to right,
      transparent 0%,
      transparent ${(fromPosition / rangeDistance) * 100}%,
      #1964A3 ${(fromPosition / rangeDistance) * 100}%,
      #1964A3 ${(toPosition / rangeDistance) * 100}%, 
      transparent ${(toPosition / rangeDistance) * 100}%, 
      transparent 100%)`
  }

  const setZIndexStacking = (currentTarget) => {
    if (Number(currentTarget.value) <= 0) {
      toValueSlider.style.zIndex = 2
    } else {
      toValueSlider.style.zIndex = 1
    }
  }

  const handleValueFromInput = (receivedValue) => {
    if (parseInt(receivedValue) >= parseInt(toValueSlider.value)) {
      fromValueSlider.value = toValueSlider.value
    } else if (parseInt(receivedValue) <= parseInt(minValue)) {
      fromValueSlider.value = minValue
    } else {
      fromValueSlider.value = receivedValue
    }

    fromValueDisplay.value = fromValueSlider.value
    fillSlider()
  }

  const handleValueToInput = (receivedValue) => {
    if (parseInt(receivedValue) <= parseInt(fromValueSlider.value)) {
      toValueSlider.value = fromValueSlider.value
    } else if (parseInt(receivedValue) >= parseInt(maxValue)) {
      toValueSlider.value = maxValue
    } else {
      toValueSlider.value = receivedValue
    }

    toValueDisplay.value = toValueSlider.value
    fillSlider()
  }

  const handleFromSliderInput = () => {
    const [from, to] = getParsedValues()

    if (from > to) {
      fromValueSlider.value = to
      fromValueDisplay.value = to
    } else {
      fromValueDisplay.value = from
    }

    fillSlider()
  }

  const handleToSliderInput = () => {
    const [from, to] = getParsedValues()

    if (from <= to) {
      toValueSlider.value = to
      toValueDisplay.value = to
    } else {
      toValueSlider.value = from
      toValueDisplay.value = from
    }

    setZIndexStacking(toValueSlider)
    fillSlider()
  }

  setDefaultValues()
  fromValueSlider.oninput = () => handleFromSliderInput()
  toValueSlider.oninput = () => handleToSliderInput()
  fromValueDisplay.addEventListener('change', (e) => {
    handleValueFromInput(e.target.value)
  })
  toValueDisplay.addEventListener('change', (e) => {
    handleValueToInput(e.target.value)
  })
  setZIndexStacking(toValueSlider)
  fillSlider()

  return () => {
    fromValueDisplay.removeEventListener('change', (e) => {
      handleValueFromInput(e.target.value)
    })
    toValueDisplay.removeEventListener('change', (e) => {
      handleValueToInput(e.target.value)
    })
  }
}
