export default function calendar() {
  ;(function ($) {
    $('input[name="vf-daterange-localised"]').daterangepicker(
      {
        locale: {
          format: 'DD/MM/YYYY',
          separator: ' - ',
          applyLabel: 'Apply',
          cancelLabel: 'Cancel',
          fromLabel: 'From',
          toLabel: 'To',
          customRangeLabel: 'Custom',
          weekLabel: 'W',
          daysOfWeek: ['S', 'M', 'T', 'W', 'T', 'F', 'S'],
          monthNames: [
            'January',
            'February',
            'March',
            'April',
            'May',
            'June',
            'July',
            'August',
            'September',
            'October',
            'November',
            'December',
          ],
          firstDay: 1,
        },
        parentEl: '.vf-calendar',
      },
      function (start, end, label) {}
    )

    $('input[name="vf-daterange-localised-svenska"]').daterangepicker(
      {
        autoApply: true,
        showWeekNumbers: true,
        locale: {
          format: 'YYYY/MM/DD',
          separator: ' - ',
          applyLabel: 'Godkänn',
          cancelLabel: 'Rensa',
          fromLabel: 'Från',
          toLabel: 'Till',
          customRangeLabel: 'Custom',
          weekLabel: 'V',
          daysOfWeek: ['Sö', 'Må', 'Ti', 'On', 'To', 'Fr', 'Lö'],
          monthNames: [
            'Januari',
            'Februari',
            'Mars',
            'April',
            'Maj',
            'Juni',
            'Juli',
            'Augusti',
            'September',
            'Oktober',
            'November',
            'December',
          ],
          firstDay: 1,
        },
        parentEl: '.vf-calendar-svenska',
        startDate: '2022/01/01',
        endDate: '2022/01/08',
      },
      function (start, end, label) {}
    )

    $('input[name="vf-daterange-single"]').daterangepicker(
      {
        singleDatePicker: true,
        autoApply: true,
        locale: {
          format: 'DD/MM/YYYY',
          separator: ' - ',
          applyLabel: 'Apply',
          cancelLabel: 'Cancel',
          fromLabel: 'From',
          toLabel: 'To',
          customRangeLabel: 'Custom',
          weekLabel: 'W',
          daysOfWeek: ['S', 'M', 'T', 'W', 'T', 'F', 'S'],
          monthNames: [
            'January',
            'February',
            'March',
            'April',
            'May',
            'June',
            'July',
            'August',
            'September',
            'October',
            'November',
            'December',
          ],
          firstDay: 1,
        },
        parentEl: '.vf-calendar-single',
      },
      function (start, end, label) {
        console.log('start: ', start)
        console.log('end: ', end)
      }
    )

    //Add Calendar icon
    $('[class^=vf-calendar]')
      .find('input')
      .each(function () {
        $(
          '<div class="vf-input-tooltip-icon vf-icon-calendar-31"></div>'
        ).insertAfter(this)
      })

    //Add classes to each cancel and apply button
    $('.vf-calendar .applyBtn').each(function () {
      $(this).addClass('vf-btn vf-btn--lg vf-btn__primary')
    })

    $('.vf-calendar .cancelBtn').each(function () {
      $(this).addClass('vf-btn vf-btn--lg vf-btn__secondary--outline')
    })
  })(jQuery)
}
