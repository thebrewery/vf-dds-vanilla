export default function singleSelect(element) {
  let singleSelectList = element.getElementsByClassName(
    'vf-visual-pickers__single-select'
  )

  const singleSelectClick = (checkmark, selectList, e) => {
    checkmark.classList.add('vf-icon-check')
    selectList.classList.add('vf-visual-pickers__outline')
    removeClassSingleSelect(e)
  }

  let singleSelectButtons = []
  for (let i = 0; i < singleSelectList.length; i++) {
    let singleSelect = singleSelectList[i].firstElementChild
    if (singleSelect.disabled == true) {
      singleSelect.parentElement.style =
        'background: #EEEEEE; pointer-events:none'
      singleSelect.nextElementSibling.style =
        'color: #767676; pointer-events:none;'
      singleSelect.nextElementSibling.style =
        'opacity:0.65; pointer-events:none'
    }
    let checkmark = singleSelect.nextElementSibling.nextElementSibling

    if (singleSelect.checked) {
      checkmark.classList.add('vf-icon-check')
      singleSelectList[i].classList.add('vf-visual-pickers__outline')
    }

    // push singleSelect buttons to array
    singleSelectButtons.push(singleSelect)

    singleSelect.addEventListener('click', function (e) {
      singleSelectClick(checkmark, singleSelectList[i], e)
    })
  }

  function removeClassSingleSelect(e) {
    // filter singleSelectbuttons by not selected ones
    const removeCheckmark = singleSelectButtons.filter((rb) => rb !== e.target)
    removeCheckmark.forEach((f) => {
      let checkmarkIcon = f.nextElementSibling.nextElementSibling
      checkmarkIcon.classList.remove('vf-icon-check')
      checkmarkIcon.parentElement.classList.remove('vf-visual-pickers__outline')
    })
  }

  return () => {
    singleSelectList.forEach((element) => {
      let checkmark = singleSelect.nextElementSibling.nextElementSibling

      element.removeEventListener('click', (e) => {
        singleSelectClick(checkmark, element, e)
      })
    })
  }
}
