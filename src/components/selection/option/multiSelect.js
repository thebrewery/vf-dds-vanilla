export default function multiSelect(element) {
  let multiSelectList = element.getElementsByClassName(
    'vf-visual-pickers__multi-select'
  )

  const multiSelectClick = (multiSelect, checkmark, element) => {
    if (multiSelect.checked == true) {
      checkmark.classList.add('vf-icon-check')
      element.classList.add('vf-visual-pickers__outline')
    } else {
      checkmark.classList.remove('vf-icon-check')
      element.classList.remove('vf-visual-pickers__outline')
    }
  }

  for (let i = 0; i < multiSelectList.length; i++) {
    let multiSelect = multiSelectList[i].firstElementChild
    let checkmark = multiSelect.nextElementSibling.nextElementSibling

    if (multiSelect.checked) {
      checkmark.classList.add('vf-icon-check')
      multiSelectList[i].classList.add('vf-visual-pickers__outline')
    }

    multiSelect.addEventListener('click', function () {
      multiSelectClick(multiSelect, checkmark, multiSelectList[i])
    })
  }

  return () => {
    multiSelectList.forEach((element) => {
      element.removeEventListener('click', () => {
        let multiSelect = element.firstElementChild
        let checkmark = element.nextElementSibling.nextElementSibling

        multiSelectClick(multiSelect, checkmark, element)
      })
    })
  }
}
