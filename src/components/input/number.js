const getSiblings = (e) => {
  let siblings = []

  if (!e.parentNode) {
    return siblings
  }

  let sibling = e.parentNode.firstChild

  while (sibling) {
    if (sibling.nodeType === 1 && sibling !== e) {
      siblings.push(sibling)
    }
    sibling = sibling.nextSibling
  }
  return siblings
}

/**
  @type { VfJsComponent } 
 */
export default function number(element) {
  const plusClick = (plusButton) => {
    const siblings = getSiblings(plusButton)

    siblings.forEach((el) => {
      if (el.tagName === 'INPUT') {
        el.value++
      }
    })
  }

  const minusClick = (minusButton) => {
    const siblings = getSiblings(minusButton)

    siblings.forEach((el) => {
      if (el.tagName === 'INPUT') {
        el.value--
      }
    })
  }

  element.querySelectorAll('#vf_number_input--up').forEach((plusButton) => {
    plusButton.addEventListener('click', () => {
      plusClick(plusButton)
    })
  })

  element.querySelectorAll('#vf_number_input--down').forEach((minusButton) => {
    minusButton.addEventListener('click', () => {
      minusClick(minusButton)
    })
  })

  return () => {
    element.querySelectorAll('#vf_number_input--up').forEach((plusButton) => {
      plusButton.removeEventListener('click', () => {
        plusClick(plusButton)
      })
    })

    element
      .querySelectorAll('#vf_number_input--down')
      .forEach((minusButton) => {
        minusButton.removeEventListener('click', () => {
          minusClick(minusButton)
        })
      })
  }
}
