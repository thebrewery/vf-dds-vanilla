//@ts-check
const updateArrayOffset = (
  /** @type {HTMLElement} */ el,
  /** @type {string} */ percentage
) => {
  el.style.strokeDasharray = parseFloat(percentage) * 2.3876 + ',238.76'
}
export default function preloader(/**@type {HTMLElement} */ element) {
  let vfSvg = document.createElementNS('http://www.w3.org/2000/svg', 'svg')
  vfSvg.setAttributeNS(null, 'class', 'vf-preloader-svg')

  let vfSvgPath = document.createElementNS('http://www.w3.org/2000/svg', 'path')
  let vfSvgPathElement = vfSvg.appendChild(vfSvgPath)
  vfSvgPathElement.setAttributeNS(null, 'class', 'vf-preloader-circle')
  vfSvgPathElement.setAttributeNS(
    null,
    'd',
    'M40 2 a 38 38 0 0 1 0 76 a 38 38 0 0 1 0 -76'
  )

  let vfSvgText = document.createElementNS('http://www.w3.org/2000/svg', 'text')
  let vfSvgTextElement = vfSvg.appendChild(vfSvgText)
  vfSvgTextElement.setAttributeNS(null, 'class', 'vf-preloader-percentage')
  vfSvgTextElement.setAttributeNS(null, 'x', '50%')
  vfSvgTextElement.setAttributeNS(null, 'y', '50%')

  element.appendChild(vfSvg)

  let displayPercentage = element.getAttribute('data-vf-preloader-percentage')

  if (!displayPercentage) {
    displayPercentage = '0'
  }

  element
    .getElementsByTagName('svg')[0]
    .getElementsByTagName('text')[0].textContent = `${displayPercentage}%`

  /** @type {HTMLElement | null} */
  const preloader = element.querySelector('.vf-preloader-circle')
  preloader && updateArrayOffset(preloader, displayPercentage)

  const config = { attributes: true }

  const callback = (
    /** @type {MutationRecord[]} */ mutationsList,
    /** @type {MutationObserver} */ observer
  ) => {
    mutationsList.map((mutation) => {
      const target = mutation.target
      if (mutation.type == 'attributes' && target instanceof HTMLElement) {
        const preloaderCircle = target.querySelector('.vf-preloader-circle')
        if (mutation.attributeName == 'data-vf-preloader-percentage') {
          target
            .getElementsByTagName('svg')[0]
            .getElementsByTagName('text')[0].textContent =
            target.getAttribute('data-vf-preloader-percentage') + '%'
          preloaderCircle instanceof HTMLElement &&
            updateArrayOffset(
              preloaderCircle,
              target.getAttribute('data-vf-preloader-percentage') || '0'
            )
        }
      }
    })
  }

  // Create an observer instance linked to the callback function
  const observer = new MutationObserver(callback)

  // Start observing the target node for configured mutations
  observer.observe(element, config)
  return () => {
    observer.disconnect()
    vfSvg.remove()
  }
}
