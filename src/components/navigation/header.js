export default function header(element) {
  const hamburgerClick = (clickedEl) => {
    element
      .querySelectorAll('.vf-header__search-box')
      .forEach((searchIcon) => searchIcon.classList.remove('active'))
    element
      .querySelectorAll('.active-search-box')
      .forEach((el) => el.classList.remove('active-search-box'))

    const menu = element.querySelector('.vf-header__menu-list')
    if (clickedEl.target.classList.contains('vf-icon-menu')) {
      clickedEl.target.classList.remove('vf-icon-menu')
      clickedEl.target.classList.add('vf-icon-close')
      menu.classList.add('active')
    } else {
      clickedEl.target.classList.add('vf-icon-menu')
      clickedEl.target.classList.remove('vf-icon-close')
      menu.classList.remove('active')
    }
  }

  element
    .querySelector('#vf-header-menu-icon')
    ?.addEventListener('click', (el) => {
      hamburgerClick(el)
    })

  const levelOneClick = (headerLevelOne) => {
    const menuLevel2 = headerLevelOne.nextElementSibling

    if (window.innerWidth > 1024) {
      element
        .querySelectorAll('.vf-header__menu-level-2.active')
        .forEach((el) => el.classList.remove('active'))
    }

    element
      .querySelectorAll('.vf-header__search-box')
      .forEach((searchIcon) => searchIcon.classList.remove('active'))

    const symbol = headerLevelOne.getElementsByTagName('span')

    if (!menuLevel2.classList.contains('active')) {
      menuLevel2.classList.add('active')

      if (window.innerWidth < 1024) {
        symbol[0].classList.remove('vf-icon-more')
        symbol[0].classList.add('vf-icon-less-info')
      }
    } else {
      if (window.innerWidth < 1024) {
        menuLevel2.classList.remove('active')
        symbol[0].classList.add('vf-icon-more')
        symbol[0].classList.remove('vf-icon-less-info')
      }
    }
  }

  element.querySelectorAll('#vf_header_level_1')?.forEach((headerLevelOne) => {
    headerLevelOne.addEventListener('click', () => {
      levelOneClick(headerLevelOne)
    })
  })

  const levelTwoClick = (headerLevelTwo) => {
    const menuLevel3 = headerLevelTwo.nextElementSibling

    const symbol = headerLevelTwo.getElementsByTagName('span')

    element
      .querySelectorAll('.vf-header__search-box')
      .forEach((searchIcon) => searchIcon.classList.remove('active'))

    if (window.innerWidth > 1024) {
      element
        .querySelectorAll('.vf-header__menu-level-3.active')
        .forEach((el) => el.classList.remove('active'))
    } else {
      if (!menuLevel3.classList.contains('active')) {
        symbol[0].classList.remove('vf-icon-more')
        symbol[0].classList.add('vf-icon-less-info')
      } else {
        symbol[0].classList.add('vf-icon-more')
        symbol[0].classList.remove('vf-icon-less-info')
      }
    }

    menuLevel3.classList.toggle('active')

    element.querySelectorAll('.arrow')?.forEach((el) => el.remove())

    const arrow = document.createElement('span')

    arrow.classList.add('arrow')

    headerLevelTwo.appendChild(arrow)
  }

  element.querySelectorAll('#vf_header_level_2')?.forEach((headerLevelTwo) => {
    headerLevelTwo.addEventListener('click', () => {
      levelTwoClick(headerLevelTwo)
    })
  })

  const searchBoxClick = (el, searchIcon) => {
    const searchEl = element.querySelector('.vf-header__search-box')
    el.target.classList.toggle('active-search-box')
    const menu = element.querySelector('#vf-header-menu-icon')
    if (!searchEl.classList.contains('active')) {
      element
        .querySelectorAll('.active')
        .forEach((el) => el.classList.remove('active'))

      if (window.innerWidth < 1024) {
        searchIcon.classList.remove('vf-icon-search')
        searchIcon.classList.add('vf-icon-close')
      }

      searchEl.classList.add('active')
    } else {
      searchIcon.classList.remove('vf-icon-close')
      searchIcon.classList.add('vf-icon-search')
      element
        .querySelectorAll('.active')
        .forEach((el) => el.classList.remove('active'))
    }

    if (window.innerWidth < 1024) {
      menu.classList.add('vf-icon-menu')
      menu.classList.remove('vf-icon-close')
    }
  }

  element.querySelectorAll('#search_box').forEach((searchIcon) =>
    searchIcon.addEventListener('click', (el) => {
      searchBoxClick(el, searchIcon)
    })
  )

  const closeClick = () => {
    element
      .querySelectorAll('.active')
      .forEach((el) => el.classList.remove('active'))
    element
      .querySelector('.active-search-box')
      .classList.remove('active-search-box')
  }

  element
    .querySelector('.vf-header__icon-close-desktop')
    ?.addEventListener('click', () => {
      closeClick()
    })

  return () => {
    element
      .querySelector('#vf-header-menu-icon')
      ?.removeEventListener('click', hamburgerClick)

    element
      .querySelectorAll('#vf_header_level_1')
      ?.forEach((headerLevelOne) => {
        headerLevelOne.removeEventListener('click', levelOneClick)
      })

    element
      .querySelectorAll('#vf_header_level_2')
      ?.forEach((headerLevelTwo) => {
        headerLevelTwo.removeEventListener('click', levelTwoClick)
      })

    element
      .querySelector('.vf-header__icon-close-desktop')
      ?.removeEventListener('click', closeClick)
  }
}
