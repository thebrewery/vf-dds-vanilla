export default function tabs(element) {
  const vfTabBarItems = element.querySelectorAll('.vf-tab-bar-item')
  const tabItems = element.querySelectorAll('.vf-tab-bar-item')

  vfTabBarItems.forEach((item) => {
    if (item.classList.contains('vf-tab-bar-item--active')) {
      const link = item.querySelector('.vf-tab-bar-link').cloneNode(true)
      link.classList.add(
        'vf-tab-bar-link-mobile',
        'vf-tab-bar-link-mobile--active'
      )
      link.classList.remove('vf-tab-bar-link')
      link.removeAttribute('aria-selected')
      link.removeAttribute('tabindex')
      item.insertAdjacentElement('afterend', link)
    } else {
      const link = item.querySelector('.vf-tab-bar-link').cloneNode(true)
      link.classList.add('vf-tab-bar-link-mobile')
      link.classList.remove('vf-tab-bar-link')
      link.removeAttribute('aria-selected')
      link.removeAttribute('tabindex')
      item.insertAdjacentElement('afterend', link)
    }
  })

  tabItems.forEach((tabItem) => {
    tabItem.removeEventListener('click', () => handleClick(tabItem))
    tabItem.removeEventListener('keyup', () => handleClick(tabItem))
    tabItem.addEventListener('click', () => handleClick(tabItem))
    tabItem.addEventListener('keyup', () => handleClick(tabItem))
  })

  function handleClick(tabBarItem) {
    if (!tabBarItem.classList.contains('vf-tab-bar-item--disabled')) {
      tabBarItem.classList.add('vf-tab-bar-item--active')
      tabBarItem.setAttribute('aria-selected', 'true')
      tabBarItem.setAttribute('tabindex', '0')
      const siblings = Array.from(tabBarItem.parentNode.children).filter(
        (child) => child !== tabBarItem
      )
      siblings.forEach((sibling) => {
        sibling.classList.remove('vf-tab-bar-item--active')
        sibling.setAttribute('aria-selected', 'false')
        sibling.setAttribute('tabindex', '-1')
      })

      const mobileLink = tabBarItem.nextElementSibling
      if (mobileLink.classList.contains('vf-tab-bar-link-mobile')) {
        mobileLink.classList.add('vf-tab-bar-link-mobile--active')
        const mobileLinkSiblings = Array.from(
          mobileLink.parentNode.children
        ).filter((child) => child !== mobileLink)
        mobileLinkSiblings.forEach((sibling) => {
          if (sibling.classList.contains('vf-tab-bar-link-mobile')) {
            sibling.classList.remove('vf-tab-bar-link-mobile--active')
          }
        })
      }

      const tabPanelContainer = tabBarItem.closest('.vf-tab-bar-container')
      const tabPanels = tabPanelContainer.querySelectorAll('[role="tabpanel"]')
      tabPanels.forEach((tabPanel) => tabPanel.setAttribute('hidden', true))

      const tabPanelToShow = tabPanelContainer.querySelector(
        `#${tabBarItem.getAttribute('aria-controls')}`
      )
      if (tabPanelToShow) {
        tabPanelToShow.removeAttribute('hidden')
      }
    }
  }

  const mobileTabsFixed = element.getElementsByClassName(
    'vf-tab-bar-item--mobile--fixed'
  )
  const mobileTabsScroll = element.getElementsByClassName(
    'vf-tab-bar-item--mobile--scroll'
  )

  const mobileTabsContentFixed = element.getElementsByClassName(
    'vf-tab-bar-content--mobile--fixed'
  )

  const mobileTabsContentScroll = element.getElementsByClassName(
    'vf-tab-bar-content--mobile--scroll'
  )

  function toggleTabs(mobileTabs, mobileTabsContent) {
    for (let i = 0; i < mobileTabs.length; i++) {
      mobileTabs[i].addEventListener('click', function (ev) {
        let siblings = ev.target.parentElement.children

        // remove active class from all tabs
        for (let sibling of siblings) {
          sibling.classList.remove('vf-tab-bar-item--mobile--active')
        }

        // hide all tab contents
        for (let content of mobileTabsContent) {
          content.style = 'display:none;'
        }

        // add active class to selected
        ev.target.classList.add('vf-tab-bar-item--mobile--active')

        // display selected tab content
        mobileTabsContent[i].style = 'display:block;'
        // mobileTabContentScroll[i].style = 'display:block;'
      })

      let isActive = mobileTabs[i].classList.contains(
        'vf-tab-bar-item--mobile--active'
      )

      if (isActive) {
        mobileTabsContent[i].style = 'display:block;'
      }
    }
  }

  toggleTabs(mobileTabsFixed, mobileTabsContentFixed)
  toggleTabs(mobileTabsScroll, mobileTabsContentScroll)

  return () => {
    tabItems.forEach((tabItem) => {
      tabItem.removeEventListener('click', () => handleClick(tabItem))
      tabItem.removeEventListener('keyup', () => handleClick(tabItem))
    })
  }
}
