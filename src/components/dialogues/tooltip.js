//@ts-check
export default function tooltip(/**@type {HTMLElement} */ element) {
  let tooltip = element.dataset.tooltip || ''
  let icon = element.dataset.icon
  if (element.parentNode == null) {
    return () => {}
  }
  //Pseudo-elements are not allowed on input elements, so we have to manually create divs.
  if (element.tagName === 'INPUT') {
    const tooltipMessage = document.createElement('div')
    const inputTooltipIcon = document.createElement('div')
    if (icon) {
      tooltipMessage.classList.add('vf-tooltip-message')
      tooltipMessage.innerHTML = tooltip
      element.parentNode.insertBefore(tooltipMessage, element.nextSibling)

      inputTooltipIcon.classList.add('vf-input-tooltip-icon', icon)
      element.parentNode.insertBefore(inputTooltipIcon, element.nextSibling)
    } else {
      tooltipMessage.classList.add('vf-tooltip-message')
      tooltipMessage.innerHTML = tooltip
      element.parentNode.insertBefore(tooltipMessage, element.nextSibling)

      inputTooltipIcon.classList.add('vf-input-tooltip-icon', 'vf-icon-help')
      element.parentNode.insertBefore(inputTooltipIcon, element.nextSibling)
    }
    return () => {
      tooltipMessage.remove()
      inputTooltipIcon.remove()
    }
  } else {
    let tooltipMessage = document.createElement('div')
    tooltipMessage.classList.add('vf-tooltip-message')
    tooltipMessage.innerHTML = tooltip
    element.appendChild(tooltipMessage)
    return () => {
      tooltipMessage.remove()
    }
  }
}
