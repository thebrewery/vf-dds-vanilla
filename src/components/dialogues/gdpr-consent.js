//@ts-check
export default function gdprConsent(/** @type {Element} */ element) {
  const expandButton = element.querySelector('#expand-button')

  const clickHandler = () => {
    const el = element.querySelector('.vf-gdpr-consent__details-content')
    el && el.classList.toggle('vf-gdpr-consent__hidden')
  }
  expandButton && expandButton.addEventListener('click', clickHandler)
  return () => {
    expandButton && expandButton.removeEventListener('click', clickHandler)
  }
}
