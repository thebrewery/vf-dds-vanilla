//@ts-check
export default function notification(/**@type {HTMLElement} */ element) {
  const listener = () => {
    const body = element.querySelector('#vf-notification__body')
    body && body.classList.toggle('vf-notification--show')
  }
  const clickElem = element.querySelector('#vf-notification__btn')
  clickElem && clickElem.addEventListener('click', listener)
  return () => {
    clickElem && clickElem.removeEventListener('click', listener)
  }
}
