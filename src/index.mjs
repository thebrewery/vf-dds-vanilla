import moduleMap from './module-map.js'

// Meant to run after load, to process all the js after DOMContentLoaded

window.addEventListener('DOMContentLoaded', () => {
  Object.keys(moduleMap).forEach((key) => {
    const initiators = document.querySelectorAll(key)

    initiators.forEach((singleInitiator) => {
      moduleMap[key](singleInitiator)
    })
  })
})
