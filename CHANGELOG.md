# Changelog

## 0.14.1 (BREAKING CHANGE)

- Updates all JavaScript to be able to be selectively executed, with a updated pattern for backwards compability
- Adds Angular example

## 0.13.0

- Updates option, tabs, tooltip & progress-indicator to not use jQuery
- Minor styling fixes

## 0.12.8

- Use local lottie file in spinner example instead of Lottie Files import.

## 0.10.1

- Fixed some minor styling bugs related to inputs and updated text about requirements to keep our inputs JS-free.

## 0.10.0

- Corrected sass linting errors.
- Stripped out non src files from NPM distribution.
