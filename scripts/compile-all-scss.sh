colours=colours/colours.scss

# DIALOGUES
gdpr=dialogues/gdpr-consent.scss
messageBanner=dialogues/message-banners.scss
notification=dialogues/notification.scss
tooltip=dialogues/tooltip.scss

# ICONS
icons=icons/icons.scss


# INDICATORS
badge=indicators/badge.scss
pagination=indicators/pagination.scss
preloader=indicators/preloader.scss
progressIndicator=indicators/progress-indicator.scss
progress=indicators/progress.scss
spinner=indicators/spinner.scss

# Campaign assets (THE EDIT)
# animatedKeyNumber=campaign-assets/animated-key-number.scss
# highlightInformationContainerBlock=campaign-assets/highlight-information-container-block.scss
# textHeroBlock=campaign-assets/text-hero-block.scss
theEdit=campaign-assets/the-edit.scss


# INPUT
input=input/input.scss

# LOGOS
topBar=logos/top-bar.scss

# MODULES
article=modules/article/article.scss
carousel=modules/carousel.scss
expoCarousel=modules/expo-carousel.scss
footer=modules/footer.scss
form=modules/form.scss
image=modules/image.scss
imageModule=modules/image-module.scss
linkedList=modules/linked-list.scss
media=modules/media.scss
offsetImage=modules/offset-image.scss
quote=modules/quote.scss
subscription=modules/subscription.scss
upload=modules/upload.scss
hero=modules/hero.scss

# NAVIGATION 
header=navigation/header.scss
tabs=navigation/tabs.scss
textLink=navigation/text-links.scss

# SELECTION
buttons=selection/buttons.scss
calendar=selection/calendar.scss
dropdown=selection/dropdown.scss
option=selection/option.scss
checkbox=selection/checkbox.scss
radiobutton=selection/radio-button.scss

# STRUCTURE
grid=structure/grid.scss
responsive=structure/responsive.scss
sizing=structure/sizing.scss
spacing=structure/spacing.scss
dividers=structure/dividers.scss
alternateGrid=structure/alternate-grid.scss

# TABLES AND LISTS
linkList=tables-and-lists/link-list.scss
numberedList=tables-and-lists/numbered-list.scss
standardList=tables-and-lists/standard-list.scss
table=tables-and-lists/table.scss

# TYPOGRAPHY
bodyCopy=typography/body-copy.scss
emphasis=typography/emphasis.scss
errors=typography/errors.scss
headers=typography/headers.scss
intro=typography/intro.scss
otherTextStyles=typography/other-text-styles.scss

# CATEGORIES
colours=/colours/colours.scss
dialogues=/dialogues/dialogues.scss
icons=/icons/icons.scss
indicators=/indicators/indicators.scss
input=/input/input.scss
logos=/logos/logos.scss
modules=/modules/modules.scss
navigation=/navigation/navigation.scss
selection=/selection/selection.scss
structure=/structure/structure.scss
tables=/tables-and-lists/tables-and-lists.scss
typography=/typography/typography.scss
fonts=fonts.scss
default=default.scss

# Utilities and mixins
mixins=mixins.scss
utilities=utilities.scss

# yarn sass --no-source-map  src/components/style/$colours:dist/css/component-css/colours.css src/components/style/$gdpr:dist/css/component-css/gdpr.css src/components/style/$messageBanner:dist/css/component-css/message-banners.css src/components/style/$notification:dist/css/component-css/notification.css src/components/style/$tooltip:dist/css/component-css/tooltip.css src/components/style/$icons:dist/css/component-css/icons.css src/components/style/$image:dist/css/component-css/image.css src/components/style/$badge:dist/css/component-css/badge.css src/components/style/$pagination:dist/css/component-css/pagination.css  src/components/style/$preloader:dist/css/component-css/preloader.css  src/components/style/$progressIndicator:dist/css/component-css/progress-indicator.css src/components/style/$progress:dist/css/component-css/progress.css  src/components/style/$spinner:dist/css/component-css/spinner.css src/components/style/$input:dist/css/component-css/input.css  src/components/style/$topBar:dist/css/component-css/top-bar.css src/components/style/$article:dist/css/component-css/article.css src/components/style/$carousel:dist/css/component-css/carousel.css src/components/style/$footer:dist/css/component-css/footer.css src/components/style/$form:dist/css/component-css/form.css src/components/style/$imageModule:dist/css/component-css/image-module.css src/components/style/$linkedList:dist/css/component-css/linked-list.css src/components/style/$media:dist/css/component-css/media.css src/components/style/$offsetImage:dist/css/component-css/offset-image.css src/components/style/$quote:dist/css/component-css/quote.css src/components/style/$subscription:dist/css/component-css/subscription.css src/components/style/$upload:dist/css/component-css/upload.css src/components/style/$header:dist/css/component-css/header.css src/components/style/$tabs:dist/css/component-css/tabs.css src/components/style/$textLink:dist/css/component-css/text-links.css src/components/style/$buttons:dist/css/component-css/buttons.css src/components/style/$calendar:dist/css/component-css/calendar.css src/components/style/$dropdown:dist/css/component-css/dropdown.css src/components/style/$option:dist/css/component-css/option.css src/components/style/$grid:dist/css/component-css/grid.css src/components/style/$responsive:dist/css/component-css/responsive.css src/components/style/$sizing:dist/css/component-css/sizing.css src/components/style/$spacing:dist/css/component-css/spacing.css src/components/style/$linkList:dist/css/component-css/link-list.css src/components/style/$numberedList:dist/css/component-css/numbered-list.css src/components/style/$standardList:dist/css/component-css/text-list.css src/components/style/$table:dist/css/component-css/table.css src/components/style/$bodyCopy:dist/css/component-css/body-copy.css src/components/style/$emphasis:dist/css/component-css/emphasis.css src/components/style/$errors:dist/css/component-css/errors.css src/components/style/$headers:dist/css/component-css/headers.css src/components/style/$intro:dist/css/component-css/intro.css src/components/style/$otherTextStyles:dist/css/component-css/other-text-styles.css  src/components/style/$hero:dist/css/component-css/hero.css
npx sass --no-source-map  src/components/style/$alternateGrid:src/style/component-css/alternate-grid.css src/style/$fonts:src/style/fonts.css src/components/style/$default:src/style/standard.css src/components/style/$colours:src/style/component-css/colours.css src/components/style/$gdpr:src/style/component-css/gdpr-consent.css src/components/style/$messageBanner:src/style/component-css/message-banners.css src/components/style/$notification:src/style/component-css/notification.css src/components/style/$tooltip:src/style/component-css/tooltip.css src/components/style/$icons:src/style/component-css/icons.css src/components/style/$image:src/style/component-css/image.css src/components/style/$badge:src/style/component-css/badge.css src/components/style/$pagination:src/style/component-css/pagination.css  src/components/style/$preloader:src/style/component-css/preloader.css  src/components/style/$progressIndicator:src/style/component-css/progress-indicator.css src/components/style/$progress:src/style/component-css/progress.css  src/components/style/$spinner:src/style/component-css/spinner.css src/components/style/$input:src/style/component-css/input.css  src/components/style/$topBar:src/style/component-css/top-bar.css src/components/style/$article:src/style/component-css/article.css src/components/style/$carousel:src/style/component-css/carousel.css src/components/style/$expoCarousel:src/style/component-css/expo-carousel.css src/components/style/$footer:src/style/component-css/footer.css src/components/style/$form:src/style/component-css/form.css src/components/style/$imageModule:src/style/component-css/image-module.css src/components/style/$linkedList:src/style/component-css/linked-list.css src/components/style/$media:src/style/component-css/media.css src/components/style/$offsetImage:src/style/component-css/offset-image.css src/components/style/$quote:src/style/component-css/quote.css src/components/style/$subscription:src/style/component-css/subscription.css src/components/style/$upload:src/style/component-css/upload.css src/components/style/$header:src/style/component-css/header.css src/components/style/$tabs:src/style/component-css/tabs.css src/components/style/$textLink:src/style/component-css/text-links.css src/components/style/$buttons:src/style/component-css/buttons.css src/components/style/$calendar:src/style/component-css/calendar.css src/components/style/$dropdown:src/style/component-css/dropdown.css src/components/style/$option:src/style/component-css/option.css src/components/style/$grid:src/style/component-css/grid.css src/components/style/$responsive:src/style/component-css/responsive.css src/components/style/$sizing:src/style/component-css/sizing.css src/components/style/$spacing:src/style/component-css/spacing.css src/components/style/$linkList:src/style/component-css/link-list.css src/components/style/$numberedList:src/style/component-css/numbered-list.css src/components/style/$standardList:src/style/component-css/text-list.css src/components/style/$table:src/style/component-css/table.css src/components/style/$bodyCopy:src/style/component-css/body-copy.css src/components/style/$emphasis:src/style/component-css/emphasis.css src/components/style/$errors:src/style/component-css/errors.css src/components/style/$headers:src/style/component-css/headers.css src/components/style/$intro:src/style/component-css/intro.css src/components/style/$otherTextStyles:src/style/component-css/other-text-styles.css  src/components/style/$hero:src/style/component-css/hero.css src/components/style/$checkbox:src/style/component-css/checkbox.css src/components/style/$radiobutton:src/style/component-css/radio-button.css src/components/style/$dividers:src/style/component-css/dividers.css src/components/style/$utilities:src/style/utilities.css src/components/style/$mixins:src/style/mixins.css src/components/style/$theEdit:src/style/component-css/the-edit.css 
# yarn sass --no-source-map  src/components/style/$colours:dist/css/category-css/colours.css src/components/style/$dialogues:dist/css/category-css/dialogues.css src/components/style/$icons:dist/css/category-css/icons.css src/components/style/$indicators:dist/css/category-css/indicators.css src/components/style/$input:dist/css/category-css/input.css src/components/style/$logos:dist/css/category-css/logos.css src/components/style/$modules:dist/css/category-css/modules.css src/components/style/$navigation:dist/css/category-css/navigation.css src/components/style/$selection:dist/css/category-css/selection.css src/components/style/$structure:dist/css/category-css/structure.css src/components/style/$tables:dist/css/category-css/tables-and-lists.css src/components/style/$typography:dist/css/category-css/typography.css 
cp src/style/component-css/input.css src/style/component-css/field.css
cp src/style/component-css/input.css src/style/component-css/textarea.css
cp src/style/component-css/input.css src/style/component-css/number.css
# src/components/style/$animatedKeyNumber:src/style/component-css/animated-key-number.css src/components/style/$highlightInformationContainerBlock:src/style/component-css/highlight-information-container-block.css src/components/style/$textHeroBlock:src/style/component-css/text-hero-block.css