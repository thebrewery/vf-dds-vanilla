const plugin = require('tailwindcss/plugin')

module.exports = {
  theme: {
    extend: {
      colors: {
        transparent: 'transparent',
        current: 'currentColor',
        primary: {
          'solar-yellow': {
            DEFAULT: '#ffda00',
            70: '#FEE570',
            50: '#FEEC9D',
            30: '#FEF4C6',
            10: '#FEFBEC',
          },
          'coal-black': '#000000',
          'aura-white': '#ffffff',
          'ocean-blue': {
            DEFAULT: '#2071b5',
            70: '#6C9ECC',
            50: '#99BBDB',
            30: '#C2D7E9',
            10: '#EBF1F8',
          },
          'magnetic-grey': {
            DEFAULT: '#4e4b48',
            70: '#878583',
            50: '#ABAAA9',
            30: '#CECDCC',
            10: '##EFEFEE',
          },
        },
        secondary: {
          'dark-green': {
            DEFAULT: '#005c63',
            70: '#5C9095',
            50: '#8EB1B5',
            30: '#BDD0D2',
            10: '#E9EFF0',
          },
          'dark-blue': {
            DEFAULT: '#1e324f',
            70: '#6B7688',
            50: '#989FAC',
            30: '#C2C6CD',
            10: '#EBECEF',
          },
          pink: {
            DEFAULT: '#d1266b',
            70: '#DF709B',
            50: '#E89BB9',
            30: '#F1C4D5',
            10: '#FAECF1',
          },
          'dark-purple': {
            DEFAULT: '#85254b',
            70: '#AB6F86',
            50: '#C39AAA',
            30: '#DCC4CD',
            10: '#F3EBEF',
          },
          green: {
            DEFAULT: '#3dc07c',
            70: '#7ED3A5',
            50: '#A5E0C0',
            30: '#CAECD9',
            10: '#EDF8F3',
          },
          red: {
            DEFAULT: '#f93b18',
            70: '#FA7C6D',
            50: '#FCA49A',
            30: '#FDC9C4',
            10: '#FEEDEC',
          },
          purple: {
            DEFAULT: '#9b62c3',
            70: '#B994D5',
            50: '#CEB4E1',
            30: '#E2D2ED',
            10: '#F5F0F9',
          },
        },
        'bg-colours': {
          'light-yellow': {
            DEFAULT: '#fffee5',
            70: '#FFFEED',
            50: '#FFFEF2',
            30: '#FFFFF8',
            10: '#FFFFFC',
          },
          'light-green': {
            DEFAULT: '#edf9f3',
            70: '#F2FBF6',
            50: '#F5FCF9',
            30: '#F9FCFB',
            10: '#FCFEFD',
          },
          'light-blue-alt': {
            DEFAULT: '#ebf2f3',
            70: '#F0F5F6',
            50: '#F5F8F8',
            30: '#F9FBFB',
            10: '#FCFDFD',
          },
          'light-blue': {
            DEFAULT: '#edf1f6',
            70: '#F2F5F8',
            50: '#F5F7FA',
            30: '#F9FBFC',
            10: '#FCFDFD',
          },
          'light-red': {
            DEFAULT: '#fef0ea',
            70: '#FEF4F0',
            50: '#FEF7F4',
            30: '#FEFAF8',
            10: '#FEFDFC',
          },
          'light-grey': {
            DEFAULT: '#f2f2f2',
            70: '#F5F5F5',
            50: '#F8F8F8',
            30: '#FBFBFB',
            10: '#FDFDFD',
          },
        },
        tertiary: {
          'ash-blue': {
            DEFAULT: '#869bad',
            70: '#ABBAC5',
            50: '#C4CED6',
            30: '#DCE1E7',
            10: '#F3F5F6',
          },
          'dark-ash-blue': {
            DEFAULT: '#69788c',
            70: '#99A2AF',
            50: '#B7BDC7',
            30: '#D4D8DD',
            10: '#F0F2F4',
          },
          'lighter-grey': {
            DEFAULT: '#e6e6e6',
            70: '#EDEDED',
            50: '#F2F2F2',
            30: '#F7F7F7',
            10: '#FCFCFC',
          },
          'light-grey': {
            DEFAULT: '#cccccc',
            70: '#DBDBDB',
            50: '#E6E6E6',
            30: '#EFEFEF',
            10: '#FAFAFA',
          },
          'medium-grey': {
            DEFAULT: '#999999',
            70: '#B9B9B9',
            50: '#CDCDCD',
            30: '#E1E1E1',
            10: '#F5F5F5',
          },
          'medium-dark-grey': {
            DEFAULT: '#767676',
            70: '#A1A1A1',
            50: '#BCBCBC',
            30: '#D8D8D8',
            10: '#F2F2F2',
          },
          'dark-grey': {
            DEFAULT: '#666666',
            70: '#979797',
            50: '#B6B6B6',
            30: '#D3D3D3',
            10: '#F0F0F0',
          },
        },
        'energy-source': {
          hydro: {
            DEFAULT: '#2da55d',
            70: '#6CC08D',
            50: '#96D2AE',
            30: '#C0E4CE',
            10: '#EAF6EE',
          },
          'chateau-green': {
            50: '#f5fbf7',
            100: '#eaf6ef',
            200: '#cbe9d7',
            300: '#abdbbe',
            400: '#6cc08e',
            500: '#2da55d',
            600: '#299554',
            700: '#227c46',
            800: '#1b6338',
            900: '#16512e',
          },
          wind: {
            DEFAULT: '#4fcc51',
            70: '#83DB85',
            50: '#A7E5A8',
            30: '#CAEFCA',
            10: '#EDF9ED',
          },
          solar: {
            DEFAULT: '#81e0a8',
            70: '#A6E9C2',
            50: '#C0EFD4',
            30: '#D9F5E4',
            10: '#F2FBF6',
          },
          biomass: {
            DEFAULT: '#375e4e',
            70: '#738E83',
            50: '#9BAEA6',
            30: '#C3CEC9',
            10: '#EBEEED',
          },
          coal: {
            DEFAULT: '#e88a74',
            70: '#EEAC9D',
            50: '#F3C4B9',
            30: '#F8DBD5',
            10: '#FCF3F1',
          },
          gas: {
            DEFAULT: '#d85067',
            70: '#E38394',
            50: '#EBA7B3',
            30: '#F3CAD1',
            10: '#FBEDEF',
          },
          nuclear: {
            DEFAULT: '#e6e6e6',
            70: '#63778E',
            50: '#909EAE',
            30: '#BCC4CE',
            10: '#E8EBEE',
          },
          'district-heating': {
            DEFAULT: '#a376cc',
            70: '#BE9FDB',
            50: '#D1BAE5',
            30: '#E3D5EF',
            10: '#F5F1F9',
          },
        },
      },
      borderColor: (theme) => ({
        ...theme('colors'),
        DEFAULT: theme('colors.tertiary.lightergray', 'currentColor'),
      }),
      fontSize: {
        // 14px/23px
        detail: ['0.875rem', '1.4375rem'],

        // 16px/28px
        bodycopy: ['1rem', '1.75rem'],
        // 20px/36px
        'bodycopy-lg': ['1.25rem', '2.25rem'],

        // 20px/28px
        'preamble-mobile': ['1.25rem', '1.75rem'],
        // 24px/36px
        preamble: ['1.5rem', '2.25rem'],

        // 18px/24px
        'heading-xs-mobile': ['1.125rem', '1.5rem'],
        // 22px/28px
        'heading-xs': ['1.375rem', '1.75rem'],

        // 22px/28px
        'heading-sm-mobile': ['1.5rem', '1.75rem'],
        // 28px/36px
        'heading-sm': ['1.75rem', '2.25rem'],

        // 28px/32px
        'heading-md-mobile': ['1.75rem', '2rem'],
        // 36px/42px
        'heading-md': ['2.25rem', '2.625rem'],

        // 34px/40px
        'heading-lg-mobile': ['2.125rem', '2.5rem'],
        // 52px/62px
        'heading-lg': ['3.25rem', '3.875rem'],

        // 44px/48px
        'heading-xl-mobile': ['2.75rem', '3rem'],
        // 60px/75px
        'heading-xl-tablet': ['3.75rem', '4.6875rem'],
        // 72px/84px
        'heading-xl': ['4.5rem', '5.25rem'],
      },
      fontFamily: {
        DEFAULT: ['Vattenfall Hall', 'Helvetica', 'sans-serif'],
        display: ['Vattenfall Hall Display', 'Helvetica', 'sans-serif'],
        icons: 'vf-icons',
      },
      fontWeight: {
        light: '300',
        regular: '400',
        medium: '500',
        bold: '600',
        heavy: '900',
      },
      screens: {
        xs: '375px',
        sm: '599px',
        md: '768px',
        lg: '1024px',
        xl: '1440px',
      },
      height: {
        screen: '100vh',
      },
      width: {
        screen: '100vw',
      },
      spacing: {
        // 28px
        sm: '1.75rem',
        // 44px
        md: '2.75rem',
        // 56px
        lg: '3.5rem',
        // 88px
        xl: '5.5px',
      },
      ease: {
        DEFAULT: 'cubic-bezier(.72,.08,.31,.9)',
      },
    },
  },
  plugins: [
    plugin(({ addBase, addUtilities, addComponents, theme }) => {
      addBase({
        '@font-face': {
          src: 'url("~@vf-dds/vf-dds-vanilla/src/style/fonts/VattenfallHall-Light.woff2")',
          fontFamily: 'Vattenfall Hall',
          fontWeight: theme('fontWeight.light'),
          fontStyle: 'normal',
        },
      })
      addBase({
        '@font-face': {
          src: 'url("~@vf-dds/vf-dds-vanilla/src/style/fonts/VattenfallHall-LightItalic.woff2")',
          fontFamily: 'Vattenfall Hall',
          fontWeight: theme('fontWeight.light'),
          fontStyle: 'italic',
        },
      })
      addBase({
        '@font-face': {
          src: 'url("~@vf-dds/vf-dds-vanilla/src/style/fonts/VattenfallHall-Regular.woff2")',
          fontFamily: 'Vattenfall Hall',
          fontWeight: theme('fontWeight.regular'),
          fontStyle: 'normal',
        },
      })
      addBase({
        '@font-face': {
          src: 'url("~@vf-dds/vf-dds-vanilla/src/style/fonts/VattenfallHall-RegularItalic.woff2")',
          fontFamily: 'Vattenfall Hall',
          fontWeight: theme('fontWeight.regular'),
          fontStyle: 'italic',
        },
      })
      addBase({
        '@font-face': {
          src: 'url("~@vf-dds/vf-dds-vanilla/src/style/fonts/VattenfallHall-Medium.woff2")',
          fontFamily: 'Vattenfall Hall',
          fontWeight: theme('fontWeight.medium'),
          fontStyle: 'normal',
        },
      })
      addBase({
        '@font-face': {
          src: 'url("~@vf-dds/vf-dds-vanilla/src/style/fonts/VattenfallHall-MediumItalic.woff2")',
          fontFamily: 'Vattenfall Hall',
          fontWeight: theme('fontWeight.medium'),
          fontStyle: 'italic',
        },
      })
      addBase({
        '@font-face': {
          src: 'url("~@vf-dds/vf-dds-vanilla/src/style/fonts/VattenfallHall-Bold.woff2")',
          fontFamily: 'Vattenfall Hall',
          fontWeight: theme('fontWeight.bold'),
          fontStyle: 'normal',
        },
      })
      addBase({
        '@font-face': {
          src: 'url("~@vf-dds/vf-dds-vanilla/src/style/fonts/VattenfallHall-BoldItalic.woff2")',
          fontFamily: 'Vattenfall Hall',
          fontWeight: theme('fontWeight.bold'),
          fontStyle: 'italic',
        },
      })
      addBase({
        '@font-face': {
          src: 'url("~@vf-dds/vf-dds-vanilla/src/style/fonts/VattenfallHall-Heavy.woff2")',
          fontFamily: 'Vattenfall Hall',
          fontWeight: theme('fontWeight.heavy'),
          fontStyle: 'normal',
        },
      })
      addBase({
        '@font-face': {
          src: 'url("~@vf-dds/vf-dds-vanilla/src/style/fonts/VattenfallHall-HeavyItalic.woff2")',
          fontFamily: 'Vattenfall Hall',
          fontWeight: theme('fontWeight.heavy'),
          fontStyle: 'italic',
        },
      })
      addBase({
        '@font-face': {
          src: 'url("~@vf-dds/vf-dds-vanilla/src/style/fonts/VattenfallHallDisplay-Bold.woff2")',
          fontFamily: 'Vattenfall Hall Display',
          fontWeight: theme('fontWeight.bold'),
          fontStyle: 'normal',
        },
      })
      addBase({
        html: {
          fontSize: '16px',
        },
        '*': {
          fontFamily: "'Vattenfall Hall', system-ui, sans-serif",
          fontStyle: 'normal',
        },
        a: {
          color: '#2071b5',
          '&:hover': {
            color: 'inherit',
          },
        },

        '.bg-active': {
          backgroundColor: ' #1964a3 !important',
        },
      })
      addUtilities({
        '.scrollbar-hide': {
          /* IE and Edge */
          '-ms-overflow-style': 'none',
          /* Firefox */
          'scrollbar-width': 'none',
        },

        '.scrollbar-hide::-webkit-scrollbar': {
          display: 'none',
        },
      })
      addComponents({
        '.vf-input__value-slider[type="range"]': {
          '-webkit-appearance': 'none',
          appearance: 'none',
          backgroundColor: 'transparent',
          borderRadius: '1.5px',
          height: '3px',
          position: 'absolute',
          pointerEvents: 'none',
          top: '-1px',
          width: 'calc(100% - 26px)',
        },
        '.vf-input__value-slider[type="range"]::-webkit-slider-thumb': {
          backgroundImage: `url("data:image/svg+xml,%0A%3Csvg width='18px' height='12px' viewBox='0 0 18 12' version='1.1' xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink'%3E%3Cdefs%3E%3Cfilter x='-23.2%25' y='-23.2%25' width='146.4%25' height='146.4%25' filterUnits='objectBoundingBox' id='filter-_t2kchbhg0-1'%3E%3CfeOffset dx='0' dy='1' in='SourceAlpha' result='shadowOffsetOuter1'%3E%3C/feOffset%3E%3CfeGaussianBlur stdDeviation='1' in='shadowOffsetOuter1' result='shadowBlurOuter1'%3E%3C/feGaussianBlur%3E%3CfeColorMatrix values='0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.15 0' type='matrix' in='shadowBlurOuter1' result='shadowMatrixOuter1'%3E%3C/feColorMatrix%3E%3CfeMerge%3E%3CfeMergeNode in='shadowMatrixOuter1'%3E%3C/feMergeNode%3E%3CfeMergeNode in='SourceGraphic'%3E%3C/feMergeNode%3E%3C/feMerge%3E%3C/filter%3E%3C/defs%3E%3Cg id='Guideline-&amp;-UI-Kit' stroke='none' stroke-width='1' fill='none' fill-rule='evenodd'%3E%3Cg id='VF-•-Guideline-•-Forms' transform='translate(-383.000000, -5190.000000)' fill='%23D8D8D8' fill-rule='nonzero'%3E%3Cg id='Input-value-range' transform='translate(155.000000, 4816.000000)'%3E%3Cg id='Range' transform='translate(0.000000, 202.000000)'%3E%3Cg id='Input-/-Value-/-Range-/-Default' transform='translate(1.000000, 126.000000)'%3E%3Cg id='Colorss-/-Primary-/-White-5' filter='url(%23filter-_t2kchbhg0-1)' transform='translate(222.000000, 37.000000)'%3E%3Cpath d='M20.6966991,14.7387961 L17.767767,17.6939806 C17.3633658,18.1020065 16.707702,18.1020065 16.3033009,17.6939806 C15.8988997,17.2859548 15.8988997,16.6244142 16.3033009,16.2163884 L18.5,14 L16.3033009,11.7836116 C15.8988997,11.3755858 15.8988997,10.7140452 16.3033009,10.3060194 C16.707702,9.89799354 17.3633658,9.89799354 17.767767,10.3060194 L20.6966991,13.2612039 C21.1011003,13.6692297 21.1011003,14.3307703 20.6966991,14.7387961 Z M7.30330086,14.7387961 L10.232233,17.6939806 C10.6366342,18.1020065 11.292298,18.1020065 11.6966991,17.6939806 C12.1011003,17.2859548 12.1011003,16.6244142 11.6966991,16.2163884 L9.5,14 L11.6966991,11.7836116 C12.1011003,11.3755858 12.1011003,10.7140452 11.6966991,10.3060194 C11.292298,9.89799354 10.6366342,9.89799354 10.232233,10.3060194 L7.30330086,13.2612039 C6.89889971,13.6692297 6.89889971,14.3307703 7.30330086,14.7387961 Z' id='Combined-Shape'%3E%3C/path%3E%3C/g%3E%3C/g%3E%3C/g%3E%3C/g%3E%3C/g%3E%3C/g%3E%3C/svg%3E")`,
          backgroundPosition: '50% 55%',
          backgroundRepeat: 'no-repeat',
          boxShadow: '0 0 0 1px #d8d8d8',
          borderRadius: '50%',
          cursor: 'pointer',
          '-webkit-appearance': 'none',
          height: '28px',
          backgroundColor: '#fff',
          position: 'relative',
          pointerEvents: 'all',
          width: '28px',
          zIndex: '2',
        },

        '.vf-input__value-slider[type="range"]::-moz-range-thumb': {
          appearance: 'none',
          '-webkit-appearance': 'none',
          pointerEvents: 'all',
          width: '28px',
          height: '28px',
          backgroundColor: '#fff',
          borderRadius: '50%',
          boxShadow: '0 0 0 1px #d8d8d8',
          cursor: 'pointer',
        },

        '.vf-input__value-slider[type="range"]::-webkit-slider-thumb:hover': {
          backgroundImage: `url("data:image/svg+xml,%0A%3Csvg width='24px' height='18px' viewBox='0 0 24 18' version='1.1' xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink'%3E%3Cdefs%3E%3Cfilter x='-42.9%25' y='-42.9%25' width='185.7%25' height='185.7%25' filterUnits='objectBoundingBox' id='filter-ivb_mmjyjw-1'%3E%3CfeOffset dx='0' dy='1' in='SourceAlpha' result='shadowOffsetOuter1'%3E%3C/feOffset%3E%3CfeGaussianBlur stdDeviation='2.5' in='shadowOffsetOuter1' result='shadowBlurOuter1'%3E%3C/feGaussianBlur%3E%3CfeColorMatrix values='0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.15 0' type='matrix' in='shadowBlurOuter1' result='shadowMatrixOuter1'%3E%3C/feColorMatrix%3E%3CfeMerge%3E%3CfeMergeNode in='shadowMatrixOuter1'%3E%3C/feMergeNode%3E%3CfeMergeNode in='SourceGraphic'%3E%3C/feMergeNode%3E%3C/feMerge%3E%3C/filter%3E%3C/defs%3E%3Cg id='Guideline-&amp;-UI-Kit' stroke='none' stroke-width='1' fill='none' fill-rule='evenodd'%3E%3Cg id='VF-•-Guideline-•-Forms' transform='translate(-459.000000, -5187.000000)' fill='%23FFFFFF' fill-rule='nonzero'%3E%3Cg id='Input-value-range' transform='translate(155.000000, 4816.000000)'%3E%3Cg id='Range' transform='translate(0.000000, 202.000000)'%3E%3Cg id='Input-/-Value-/-Range-/-Hover' transform='translate(289.000000, 126.000000)'%3E%3Cg id='Colorss-/-Primary-/-White' filter='url(%23filter-ivb_mmjyjw-1)' transform='translate(13.000000, 37.000000)'%3E%3Cpath d='M20.6966991,14.7387961 L17.767767,17.6939806 C17.3633658,18.1020065 16.707702,18.1020065 16.3033009,17.6939806 C15.8988997,17.2859548 15.8988997,16.6244142 16.3033009,16.2163884 L18.5,14 L16.3033009,11.7836116 C15.8988997,11.3755858 15.8988997,10.7140452 16.3033009,10.3060194 C16.707702,9.89799354 17.3633658,9.89799354 17.767767,10.3060194 L20.6966991,13.2612039 C21.1011003,13.6692297 21.1011003,14.3307703 20.6966991,14.7387961 Z M7.30330086,14.7387961 L10.232233,17.6939806 C10.6366342,18.1020065 11.292298,18.1020065 11.6966991,17.6939806 C12.1011003,17.2859548 12.1011003,16.6244142 11.6966991,16.2163884 L9.5,14 L11.6966991,11.7836116 C12.1011003,11.3755858 12.1011003,10.7140452 11.6966991,10.3060194 C11.292298,9.89799354 10.6366342,9.89799354 10.232233,10.3060194 L7.30330086,13.2612039 C6.89889971,13.6692297 6.89889971,14.3307703 7.30330086,14.7387961 Z' id='Combined-Shape'%3E%3C/path%3E%3C/g%3E%3C/g%3E%3C/g%3E%3C/g%3E%3C/g%3E%3C/g%3E%3C/svg%3Eä")`,
          boxShadow: '0 0 0 1px #387bbe',
          '-webkit-box-shadow': 'inset 0 0 3px #387bbe, 0 0 0px #387bbe',
          background: '#1964a3',
        },
      })
    }),
  ],
}
