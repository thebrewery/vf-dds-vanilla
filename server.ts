import type { Handler } from 'vite-plugin-mix'

import UrlPattern from 'url-pattern'
import fs from 'fs'
import fastGlob from 'fast-glob'

const basePath = '/src/examples'
const examplePath = 'src/examples'
const variantPattern = new UrlPattern(
  `${basePath}/:category/:component/:componentHtml.html`
)

export const handler: Handler = async (req, res, next) => {
  const match = variantPattern.match(req.path)
  const rootMatch = req.path === '/'

  if (rootMatch) {
    const allComponents = await fastGlob(`*/*/*.html`, {
      cwd: `${examplePath}`,
    })

    const componentHtml =
      `<div style='display: grid; grid-template-columns: repeat(4, 1fr); grid-gap: 2rem;'>` +
      allComponents
        .map(
          (component) =>
            `<a style='text-decoration: none; width: 100%;height: 200px; border: 1px solid #000;background: #fff;color: #000;display: flex;align-items: center;justify-content: center;' href='${basePath}/${component}'>${component}</a>`
        )
        .join('') +
      '</div>'

    const finalString = componentHtml.concat(
      '<script src="./src/index.mjs" type="module"></script>'
    )

    res.end(finalString)

    return
  }

  //Matches variant
  if (match) {
    const { query } = req

    let { variant } = query
    const { component, category, componentHtml } = match

    const componentPath = `${process.cwd()}${basePath}/${category}/${component}`

    const allVariants = fs.existsSync(`${componentPath}/variants`)
      ? await fastGlob(`*`, {
          onlyDirectories: true,
          cwd: `${componentPath}/variants`,
        })
      : []

    variant ??= allVariants[0]

    const variantSelect =
      allVariants.length === 0
        ? ''
        : `<select style="position: absolute; top: 0px; right: 0px;" value="${variant}" onchange="window.location = '?variant=' + event.target.value">
      ${allVariants
        .map(
          (variantOption) =>
            `<option ${
              variantOption === variant ? 'selected="selected"' : ''
            }>${variantOption}</option>`
        )
        .join('')}
    </select>`

    const htmlFile = fs.readFileSync(
      `${componentPath}/${componentHtml}.html`,
      'utf-8'
    )

    const finalFile = htmlFile.concat(
      '<script type="module" src="/src/index.mjs"></script>'
    )

    const variantFile =
      variant &&
      fs.readFileSync(
        variant
          ? `${componentPath}/variants/${variant}/index.html`
          : allVariants[0],
        'utf-8'
      )
    const joinedFile = finalFile.replace(
      '{{content}}',
      variantFile + variantSelect
    )
    return res.end(joinedFile)
  }
  next()
}
