import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
// @ts-ignore
import header from '@vf-dds/vf-dds-vanilla/src/components/navigation/header'

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css'],
})
export class HeaderComponent implements OnInit {
  @ViewChild('ref', { static: true }) ref!: ElementRef;
  ngOnInit(): void {
    header(this.ref.nativeElement);
  }
}
